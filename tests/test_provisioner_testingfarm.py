"""Test cases for testing farm module."""
import base64
from importlib.resources import files
import os
import pathlib
import unittest
from unittest import mock
from unittest.mock import patch

from requests.exceptions import RequestException
import responses

from upt.plumbing.format import ProvisionData
from upt.provisioners import testingfarm

from . import utils

ASSETS = files(__package__) / 'assets'
MOCKED_LIB = 'tf_requests.get_auto_compose.calculate_auto_compose'


class TestTestingFarmHelper(unittest.TestCase):
    """Test cases for helper functions."""

    maxDiff = None
    tf_pool = 'tf_pool_value'
    tf_hardware = 'tf_hardware_value'
    expected_call_params = {
        'RELEASE_NAME': 'stream-x/release_y',
        'ARCH': 'aarch64',
        'HW_TARGET': tf_hardware,
        'webserver_releases': 'https://toolchain.rhivos',
        "IMAGE_NAME": 'qa',
        "IMAGE_TYPE": 'regular'
    }
    os_env = {
        'AUTOMOTIVE_CONFIGURATION_URL':
            'https://example.com/in-vehicle-os/stream-x/release_y/test_images_info.json',
        'ARCH_CONFIG': 'aarch64',
        'AUTOMOTIVE_TOOLCHAIN_URL': 'https://toolchain.rhivos',
        'TF_POOL': tf_pool,
        'TF_HARDWARE': tf_hardware,
        'TF_API_TOKEN': 'MOCK_TESTING_FARM_API_TOKEN',
    }

    def setUp(self) -> None:
        self.public_key = 'base64_encoded_key'

    @mock.patch(MOCKED_LIB)
    @patch.dict(os.environ, os_env)
    def test_get_automotive_compose(self, get_auto_compose_mock):
        """Get an automotive compose."""
        testingfarm.get_automotive_compose()
        get_auto_compose_mock.assert_called_once_with(self.expected_call_params)

    @mock.patch(MOCKED_LIB)
    @patch.dict(os.environ, os_env)
    def test_get_data_with_an_automotive_compose(self, get_auto_compose_mock):
        """Validate request data for testing farm provisioning request with an automotive pool."""
        get_auto_compose_mock.return_value = ['foo_pool', 'foo_compose']
        expected = {
            'api_key': 'MOCK_TESTING_FARM_API_TOKEN',
            'test': {
                'fmf': {
                    'url': 'https://gitlab.com/testing-farm/tests',
                    'ref': 'main',
                    'name': '/testing-farm/reserve'
                }
            },
            'environments': [
                {
                    'arch': 'aarch64',
                    'os': {
                        'compose': 'foo_compose'
                    },
                    'pool': self.tf_pool,
                    'artifacts': [],
                    'variables': {
                        'TF_RESERVATION_DURATION': testingfarm.DEFAULT_TF_RESERVATION_DURATION
                    },
                    'secrets': {
                        'TF_RESERVATION_AUTHORIZED_KEYS_BASE64': self.public_key
                    }
                }
            ]
        }
        self.assertDictEqual(expected, testingfarm.get_data(self.public_key))
        get_auto_compose_mock.assert_called_once_with(self.expected_call_params)

    @patch.dict(os.environ, {
        'ARCH_CONFIG': 'aarch64',
        'TF_API_TOKEN': 'MOCK_TESTING_FARM_API_TOKEN',
        'TF_POOL': 'cki-pool',
        'OS': 'CentOS-Stream-9'
    })
    def test_get_data_without_an_automotive_compose(self):
        """Validate request data for testing farm provisioning request for OS only."""
        expected = {
            'api_key': 'MOCK_TESTING_FARM_API_TOKEN',
            'test': {
                'fmf': {
                    'url': 'https://gitlab.com/testing-farm/tests',
                    'ref': 'main',
                    'name': '/testing-farm/reserve'
                }
            },
            'environments': [
                {
                    'arch': 'aarch64',
                    'os': {
                        'compose': 'CentOS-Stream-9'
                    },
                    'pool': 'cki-pool',
                    'artifacts': [],
                    'variables': {
                        'TF_RESERVATION_DURATION': testingfarm.DEFAULT_TF_RESERVATION_DURATION
                    },
                    'secrets': {
                        'TF_RESERVATION_AUTHORIZED_KEYS_BASE64': self.public_key
                    }
                }
            ]
        }
        self.assertDictEqual(expected, testingfarm.get_data(self.public_key))

    # @patch('tf_requests.get_auto_compose.calculate_auto_compose')
    @mock.patch(MOCKED_LIB)
    @patch.dict(os.environ, os_env)
    def test_get_environment_automotive(self, get_auto_compose_mock):
        """Get an automotive environment."""
        get_auto_compose_mock.return_value = ['foo_pool', 'foo_compose']
        expected = {
            'arch': 'aarch64',
            'os': {
                'compose': 'foo_compose'
            },
            'pool': self.tf_pool,
            'artifacts': [],
            'variables': {
                'TF_RESERVATION_DURATION': testingfarm.DEFAULT_TF_RESERVATION_DURATION
            },
            'secrets': {
                'TF_RESERVATION_AUTHORIZED_KEYS_BASE64': self.public_key
            }
        }

        self.assertDictEqual(expected, testingfarm.get_environment(self.public_key))
        get_auto_compose_mock.assert_called_once_with(self.expected_call_params)

    @patch.dict(os.environ, {
        'ARCH_CONFIG': 'aarch64',
        'TF_API_TOKEN': 'MOCK_TESTING_FARM_API_TOKEN',
        'TF_POOL': 'cki-pool',
        'OS': 'CentOS-Stream-9'
    })
    def test_get_environment_no_automotive(self):
        """Get environment which is not automotive based."""
        expected = {
            'arch': 'aarch64',
            'os': {
                'compose': 'CentOS-Stream-9'
            },
            'pool': 'cki-pool',
            'artifacts': [],
            'variables': {
                'TF_RESERVATION_DURATION': testingfarm.DEFAULT_TF_RESERVATION_DURATION
            },
            'secrets': {
                'TF_RESERVATION_AUTHORIZED_KEYS_BASE64': self.public_key
            }
        }

        self.assertDictEqual(expected, testingfarm.get_environment(self.public_key))


class TestTestingFarm(unittest.TestCase):
    """Testcases for Testing Farm provisioner."""

    os_env = {
        'ARCH_CONFIG': 'aarch64',
        'TF_API_TOKEN': 'MOCK_TESTING_FARM_API_TOKEN',
        'TF_API_URL': 'https://testingfarm/api',
        'TF_POOL': 'cki-pool',
        'OS': 'CentOS-Stream-9'
    }

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS, 'testingfarm_req.yaml')
        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.testingfarm = self.provision_data.get_provisioner('testingfarm')
        self.tf_id = '163333fb-3e6a-4779-a881-ec82973hd31c'

    def test_get_public_key(self):
        """
        Test public key.

        SSH public key must start with ssh-rsa and it's base64 encoded.
        """
        public_key = self.testingfarm.get_public_key()
        decoded_public_key = base64.b64decode(public_key).decode('utf-8')
        self.assertTrue(decoded_public_key.startswith('ssh-rsa'))

    def test_write_private_key(self):
        """
        Test private key.

        SSH private key must start with -----BEGIN RSA PRIVATE KEY----- and it's base64 encoded.
        """
        with utils.create_temporary_files([]):
            private_key = pathlib.Path('foo')
            self.testingfarm.write_private_key(private_key)
            self.assertTrue(private_key.is_file())
            # Checking read and write permissions only for the owner
            self.assertEqual(33152, private_key.stat().st_mode)
            content = private_key.read_text(encoding='utf-8')
        self.assertTrue(content.startswith('-----BEGIN RSA PRIVATE KEY-----'))

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_provision_success(self):
        """Ensure provision works / is called with correct params."""
        responses.add(responses.POST, "https://testingfarm/api/requests",
                      json={"id": f"{self.tf_id}"}, status=200)
        with utils.create_temporary_files([]) as tmpdir:
            self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id
            self.testingfarm.ssh_directory = tmpdir
            self.testingfarm.provision()
            private_key = pathlib.Path(tmpdir, self.tf_id)
            self.assertTrue(private_key.is_file())
            self.assertEqual(f'-i {private_key}', self.testingfarm.rgs[0].ssh_opts)
        self.assertEqual(self.tf_id, self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'])

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_provision_fail(self):
        """Test provision failed request response."""
        responses.add(responses.POST, "https://testingfarm/api/requests",
                      body=RequestException("Network error"))
        with self.assertRaises(testingfarm.TestingFarmException) as context:
            self.testingfarm.provision()
        expected_message = 'An error occurred while making the request: Network error'
        self.assertEqual(str(context.exception), expected_message)

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_provision_fail_400(self):
        """Test provision failed request 400 response."""
        responses.add(responses.POST, "https://testingfarm/api/requests", status=400)
        with self.assertRaises(testingfarm.TestingFarmException):
            self.testingfarm.provision()

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_provisioning_state_when_the_state_is_error(self):
        """Ensure get_provision_state returns false when the status is error."""
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      json={'state': 'error'}, status=200)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id
        tf_exception_msg = 'The status of the provision is error'

        with self.assertRaisesRegex(testingfarm.TestingFarmException, tf_exception_msg):
            self.testingfarm.get_provisioning_state(self.testingfarm.rgs[0])

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_provisioning_state_when_the_state_is_not_error_or_running(self):
        """Ensure get_provision_state returns false when the status is error."""
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      json={'state': 'other'}, status=200)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id

        self.assertEqual(
            (False, [self.testingfarm.rgs[0].recipeset.hosts[0]]),
            self.testingfarm.get_provisioning_state(self.testingfarm.rgs[0]))

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_provisioning_state_success(self):
        """Ensure get_provision_state when the SUT is provisioned."""
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      json={'state': 'running',
                            'run': {'artifacts': f'http://testingfarm/api/{self.tf_id}'}},
                      status=200)
        responses.add(responses.GET, f'http://testingfarm/api/{self.tf_id}/pipeline.log',
                      body="execute task #1 2e83] Guest is ready: ArtemisGuest(9f123f7-d2-45-"
                           "ad-ea23e83, root@99.99.99.99, arch", status=200)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id
        self.assertEqual(
            (True, []),
            self.testingfarm.get_provisioning_state(self.testingfarm.rgs[0]))

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_provisioning_state_is_being_provisioned_by_testing_farm(self):
        """Ensure get_provision_state when the SUT is being provisioned by testing farm."""
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      json={'state': 'running',
                            'run': {'artifacts': f'http://testingfarm/api/{self.tf_id}'}},
                      status=200)
        responses.add(responses.GET, f'http://testingfarm/api/{self.tf_id}/pipeline.log',
                      body="some text", status=200)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id
        self.assertEqual(
            (False, [self.testingfarm.rgs[0].recipeset.hosts[0]]),
            self.testingfarm.get_provisioning_state(self.testingfarm.rgs[0]))

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_provisioning_state_ne200(self):
        """Ensure get_provision_state returns Exception on error provisioning state."""
        response_status_code = 404
        response_text = 'Testing Farm Request not found'
        tf_exception_msg = f'Response unexpected: Code {response_status_code}, Text {response_text}'
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      body=response_text, status=response_status_code)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id

        with self.assertRaisesRegex(testingfarm.TestingFarmException, tf_exception_msg):
            self.testingfarm.get_provisioning_state(self.testingfarm.rgs[0])

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_provisioning_state_raising_an_exception(self):
        """Ensure get_provision_state raise an exception with a request exception."""
        request_exception_msg = 'Some error'
        tf_exception_msg = f'An error occurred while making the request: {request_exception_msg}'
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      body=RequestException(request_exception_msg)
                      )
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id

        with self.assertRaisesRegex(testingfarm.TestingFarmException, tf_exception_msg):
            self.testingfarm.get_provisioning_state(self.testingfarm.rgs[0])

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_ip_from_pipeline_when_the_request_fails(self):
        """Ensure we're getting the exception when the request fails."""
        pipeline_url = f'http://testingfarm/api/{self.tf_id}/pipeline.log'
        request_exception_msg = 'Some error'
        tf_exception_msg = f'An error occurred while making the request: {request_exception_msg}'
        responses.add(responses.GET, pipeline_url, body=RequestException(request_exception_msg))
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id

        with self.assertRaisesRegex(testingfarm.TestingFarmException, tf_exception_msg):
            self.testingfarm.get_ip_from_pipeline(pipeline_url)

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_ip_from_pipeline_when_the_status_code_ne200(self):
        """Ensure we're getting the exception when status code is not 200."""
        pipeline_url = f'http://testingfarm/api/{self.tf_id}/pipeline.log'
        response_status_code = 404
        response_text = 'Testing Farm Request not found'
        tf_exception_msg = f'Response unexpected: Code {response_status_code}, Text {response_text}'
        responses.add(responses.GET, pipeline_url, body=response_text, status=response_status_code)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id

        with self.assertRaisesRegex(testingfarm.TestingFarmException, tf_exception_msg):
            self.testingfarm.get_ip_from_pipeline(pipeline_url)

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_ip_from_pipeline_when_ip_is_present(self):
        """Ensure we're getting the ip when it is present."""
        pipeline_url = f'http://testingfarm/api/{self.tf_id}/pipeline.log'
        resource_id = '99.99.99.99'
        response_text = (
            "execute task #1 2e83] Guest is ready: ArtemisGuest(9f123f7-d2-45-"
            f"ad-ea23e83), root@{resource_id} arch"
        )
        response_status_code = 200
        responses.add(responses.GET, pipeline_url, body=response_text, status=response_status_code)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id

        self.assertEqual(resource_id,
                         self.testingfarm.get_ip_from_pipeline(pipeline_url)
                         )

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_ip_from_pipeline_when_ip_is_not_present(self):
        """Ensure we're getting the ip when it is not present."""
        pipeline_url = f'http://testingfarm/api/{self.tf_id}/pipeline.log'
        response_text = 'Some text'
        response_status_code = 200
        responses.add(responses.GET, pipeline_url, body=response_text, status=response_status_code)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id

        self.assertIsNone(self.testingfarm.get_ip_from_pipeline(pipeline_url))

    def test_provision_rg(self):
        """Ensure provision works / is called with only one resource group."""
        self.testingfarm.rgs.append('new item')
        with self.assertRaises(Exception):
            self.testingfarm.provision()

    def test_provision_hosts(self):
        """Ensure provision works / is called with only one host per recipeset."""
        self.testingfarm.rgs[0].recipeset.hosts.append('item two')
        with self.assertRaises(Exception):
            self.testingfarm.provision()

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_is_provisioned_is_true(self):
        """Test when provisioned is true."""
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      json={'state': 'running',
                            'run': {'artifacts': f'https://testingfarm/api/{self.tf_id}'}},
                      status=200)
        responses.add(responses.GET, f'https://testingfarm/api/{self.tf_id}/pipeline.log',
                      body="execute task #1 2e83] Guest is ready: ArtemisGuest(9f123f7-d2-45-"
                           "ad-ea23e83, root@99.99.99.99, arch", status=200)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id
        self.assertTrue(self.testingfarm.is_provisioned(self.testingfarm.rgs[0]))

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_get_is_provisioned_is_false(self):
        """Test a case when provisioned is false."""
        responses.add(responses.GET, f"https://testingfarm/api/requests/{self.tf_id}",
                      json={'state': 'other'}, status=200)
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id
        self.assertFalse(self.testingfarm.is_provisioned(self.testingfarm.rgs[0]))

    @patch.dict(os.environ, os_env)
    @responses.activate
    def test_release_rg(self):
        """Test base UPT class to cancel a resource group."""
        responses.add(responses.DELETE, f"https://testingfarm/api/requests/{self.tf_id}",
                      status=200, json={'api_key': 'MOCK_TESTING_FARM_API_TOKEN'})
        self.testingfarm.rgs[0].recipeset.hosts[0].misc['resource_id'] = self.tf_id
        self.testingfarm.release_rg(self.testingfarm.rgs[0])
#
#    def test_reprovision_aborted(self):
#        """Base UPT class, noop by default, should not crash if called."""
#        self.testingfarm.reprovision_aborted(self.testingfarm.rgs[0])
#
#    def test_get_resource_ids(self):
#        """Ensure if this method is called exception is raised."""
#        with self.assertRaises(Exception):
#            self.testingfarm.get_resource_ids()
#
#    def test_heartbeat(self):
#        """Base UPT class, noop by default, should not crash if called."""
#        self.testingfarm.heartbeat(self.testingfarm.rgs[0], '1234')
#
#    def test_update_provisioning_request(self):
#        """Base UPT class, noop by default, should not crash if called."""
#        self.testingfarm.update_provisioning_request(self.testingfarm.rgs[0])
