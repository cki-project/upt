"""Utils class."""
from contextlib import contextmanager
import json
import os
import pathlib
import shutil
import tempfile
import typing

from cki_lib.kcidb.validate import PRODUCER_KCIDB_SCHEMA

KCIDB_DEFAULT_JSON = {
    'version': {'major': PRODUCER_KCIDB_SCHEMA.major, 'minor': PRODUCER_KCIDB_SCHEMA.minor},
    'checkouts': [
        {'id': 'redhat:5678', 'origin': 'redhat'},
    ],
    'builds': [
        {'id': 'redhat:1234', 'checkout_id': 'redhat:5678', 'origin': 'redhat',
         'architecture': 'x86_64'},
    ],
    'tests': [
        {
            'id': 'redhat:1234_upt_1',
            'origin': 'redhat',
            'build_id': 'redhat:1234',
            'comment': 'a3',
            'path': 'redhat_acpi_table',
            'waived': False,
            'misc': {
                'debug': False,
                'targeted': False
            }
        },
        {
            'id': 'redhat:1234_upt_2',
            'origin': 'redhat',
            'build_id': 'redhat:1234',
            'comment': 'a4',
            'path': 'some.path',
            'waived': False,
            'misc': {
                'debug': False,
                'targeted': False
            }
        }
    ]
}


def remove_spaces_and_new_lines(string: str) -> str:
    """Helpers to remove spaces and new lines from a string."""
    return string.replace('\n', '').replace(' ', '')


@contextmanager
def create_temporary_files(files: typing.Iterable[typing.Union[str, pathlib.Path]],
                           directories: typing.Iterable[typing.Union[str, pathlib.Path]] = None
                           ) -> typing.Generator:
    """Create empty temporary files and/or directories."""
    old_path = os.getcwd()
    temp_path = tempfile.mkdtemp()
    for cur_dir in directories or []:
        path = pathlib.Path(temp_path, cur_dir)
        path.mkdir(parents=True, exist_ok=True)
    for file in files:
        path = pathlib.Path(temp_path, file)
        path.parent.mkdir(parents=True, exist_ok=True)
        path.touch()
    try:
        os.chdir(temp_path)
        yield temp_path
    finally:
        os.chdir(old_path)
        shutil.rmtree(temp_path)


@contextmanager
def create_temporary_kcidb(content=None):
    """Create temporary KCIDB file with specified or default content.

    Returns: String representing the path to the KCIDB file.
    """
    kcidb_parent_dir = 'tmpdir'
    with create_temporary_files([]) as kcidb_parent_dir:
        kcidb_path = pathlib.Path(kcidb_parent_dir, 'kcidb_all.json')
        kcidb_path.write_text(json.dumps(content or KCIDB_DEFAULT_JSON))
        yield str(kcidb_path)
