"""Test cases for upt.restraint.wrapper/misc module."""
import unittest

from upt.restraint.wrapper import misc


class TestRCMisc(unittest.TestCase):
    """Test cases for upt.restraint.wrapper/misc module."""

    def test_add_directory_suffix(self):
        """Ensure add_directory_suffix works."""
        output = misc.add_directory_suffix('tests', 1)
        self.assertEqual(output, 'tests.done.01')
