"""Test cases for aws module."""
import itertools
import json
import os
import unittest
from unittest import mock

from botocore.exceptions import ClientError
from cki_lib import misc
from freezegun import freeze_time

from tests import const
from upt.const import EC2_INSTANCE_RUNNING
from upt.logger import COLORS
from upt.plumbing.format import ProvisionData
from upt.provisioners.aws import AWS
from upt.provisioners.aws import LOGGER


@mock.patch.object(AWS, "session", mock.Mock())
class TestAWS(unittest.TestCase):
    """Testcase for AWS provisioner."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(const.ASSETS_DIR, 'aws_req.yaml')
        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.aws = self.provision_data.get_provisioner('aws')

    def test_update_provisioning_request(self):
        """Ensure update_provisioning_request works and sets hostname."""
        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'instance') as mock_inst:
            mock_inst.public_dns_name = 'hostname1'

            self.aws.update_provisioning_request(self.aws.rgs[0])
            self.assertEqual(str(list(self.aws.hosts())[0].hostname), 'hostname1')

    def test_release_resources(self):
        """Ensure release_resources works with instance."""
        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'instance') as mock_inst:
            self.aws.release_resources()
            mock_inst.terminate.assert_called()

    def test_release_resources_no_instance(self):
        """Ensure release_resources works with instance."""
        with mock.patch.object(self.aws.ec2_resource, 'Instance') as mock_inst:
            mock_inst.return_value.terminate.return_value = None

            self.aws.release_resources()
            mock_inst.return_value.terminate.assert_called()

    @mock.patch.dict(
        os.environ,
        {
            "AWS_UPT_LAUNCH_TEMPLATE_NAME": "template",
            "AWS_UPT_INSTANCE_PREFIX": const.AWS_UPT_INSTANCE_PREFIX,
            "CI_PIPELINE_ID": "123",
            "CI_JOB_ID": "456",
        },
    )
    @mock.patch("time.sleep", mock.Mock())
    @mock.patch("upt.logger.LOGGER.printc")
    def test_provision(self, mock_printc):
        """Ensure provision works."""
        job_id = os.environ["CI_JOB_ID"]
        host = next(iter(self.aws.hosts()))
        host.misc = {"instance_id": None}

        with mock.patch.object(self.aws.ec2_client, "create_fleet") as mock_create_fleet:
            mock_create_fleet.side_effect = [
                # Fail twice
                {"Errors": [{"ErrorMessage": "Failed", "ErrorCode": "1"}]},
                {"Errors": [{"ErrorMessage": "Oh no", "ErrorCode": "2"}]},
                # And succeed
                {"Instances": [{"InstanceIds": ["i-12345"]}]},
            ]

            with (
                self.assertLogs(logger="root", level="WARNING") as log_root_ctx,
                self.assertLogs(logger=LOGGER, level="WARNING") as log_ctx,
            ):
                self.aws.provision()

            mock_printc.assert_called_with("Provisioned instance: i-12345", color=COLORS.GREEN)
            expected_logs = [
                f"WARNING:{LOGGER.name}:Failed to provision instance: ['Failed (ErrorCode=1)']",
                f"WARNING:{LOGGER.name}:Failed to provision instance: ['Oh no (ErrorCode=2)']",
            ]
            self.assertEqual(expected_logs, log_ctx.output)
            expected_root_logs = [
                f"WARNING:root:RETRY func({self.aws}, {host}), delaying for 3s",
                f"WARNING:root:RETRY func({self.aws}, {host}), delaying for 6s",
            ]
            self.assertEqual(expected_root_logs, log_root_ctx.output)
            instance_name = f"j{job_id}.r{host.recipe_id}.3"
            params = {
                "LaunchTemplateConfigs": [
                    {
                        "LaunchTemplateSpecification": {
                            "LaunchTemplateName": "template",
                            "Version": "$Default",
                        },
                        "Overrides": [],
                    }
                ],
                "OnDemandOptions": {},
                "TagSpecifications": [
                    {
                        "ResourceType": "instance",
                        "Tags": [
                            {
                                "Key": "Name",
                                "Value": f"{const.AWS_UPT_INSTANCE_PREFIX}.{instance_name}",
                            },
                            {"Key": "CkiGitLabPipelineId", "Value": "123"},
                            {"Key": "CkiGitLabJobId", "Value": job_id},
                        ],
                    }
                ],
                "TargetCapacitySpecification": {
                    "DefaultTargetCapacityType": "on-demand",
                    "TotalTargetCapacity": 1,
                },
                "Type": "instant",
            }
            mock_create_fleet.assert_called_with(**params)

        # serialize to json to normalize, then deserialize to compare
        serialized_result = json.dumps(
            self.aws.to_mapping(), default=lambda x: list(x) if isinstance(x, set) else x
        )
        path_to_expected = os.path.join(const.ASSETS_DIR, "expected-aws-provision.json")
        with open(path_to_expected, encoding="utf-8") as expected_file:
            self.assertEqual(json.loads(serialized_result), json.load(expected_file))

    @mock.patch.dict(
        os.environ,
        {
            "AWS_UPT_LAUNCH_TEMPLATE_NAME": "template",
            "AWS_UPT_INSTANCE_PREFIX": const.AWS_UPT_INSTANCE_PREFIX,
            "CI_PIPELINE_ID": "123",
            "CI_JOB_ID": "456",
        },
    )
    @mock.patch("time.sleep", mock.Mock())
    def test_provision_fails_graciously(self):
        """Ensure provision fails graciously after the retries are exhausted."""
        job_id = os.environ["CI_JOB_ID"]
        host = next(iter(self.aws.hosts()))
        host.misc = {"instance_id": None}

        with mock.patch.object(self.aws.ec2_client, "create_fleet") as mock_create_fleet:
            mock_create_fleet.side_effect = [
                # Exhaust retries
                {"Errors": [{"ErrorMessage": "Failed", "ErrorCode": "1"}]},
                {"Errors": [{"ErrorMessage": "Oh no", "ErrorCode": "2"}]},
                {"Errors": [{"ErrorMessage": "The end", "ErrorCode": "3"}]},
            ]

            with (
                self.assertLogs(logger="root", level="WARNING") as log_root_ctx,
                self.assertLogs(logger=LOGGER, level="WARNING") as log_ctx,
            ):
                self.aws.provision()

            expected_logs = [
                f"WARNING:{LOGGER.name}:Failed to provision instance: ['Failed (ErrorCode=1)']",
                f"WARNING:{LOGGER.name}:Failed to provision instance: ['Oh no (ErrorCode=2)']",
                f"WARNING:{LOGGER.name}:Failed to provision instance: ['The end (ErrorCode=3)']",
                (
                    f"ERROR:{LOGGER.name}:"
                    "AWSProvisionError(\"Failed to provision instance: ['The end (ErrorCode=3)']\")"
                ),
            ]
            self.assertEqual(expected_logs, log_ctx.output)
            expected_root_logs = [
                f"WARNING:root:RETRY func({self.aws}, {host}), delaying for 3s",
                f"WARNING:root:RETRY func({self.aws}, {host}), delaying for 6s",
                f"WARNING:root:RETRY giving up on func({self.aws}, {host})",
            ]
            self.assertEqual(expected_root_logs, log_root_ctx.output)

            instance_name = f"j{job_id}.r{host.recipe_id}.3"
            params = {
                "LaunchTemplateConfigs": [
                    {
                        "LaunchTemplateSpecification": {
                            "LaunchTemplateName": "template",
                            "Version": "$Default",
                        },
                        "Overrides": [],
                    }
                ],
                "OnDemandOptions": {},
                "TagSpecifications": [
                    {
                        "ResourceType": "instance",
                        "Tags": [
                            {
                                "Key": "Name",
                                "Value": f"{const.AWS_UPT_INSTANCE_PREFIX}.{instance_name}",
                            },
                            {"Key": "CkiGitLabPipelineId", "Value": "123"},
                            {"Key": "CkiGitLabJobId", "Value": job_id},
                        ],
                    }
                ],
                "TargetCapacitySpecification": {
                    "DefaultTargetCapacityType": "on-demand",
                    "TotalTargetCapacity": 1,
                },
                "Type": "instant",
            }
            mock_create_fleet.assert_called_with(**params)

        # serialize to json to normalize, then deserialize to compare
        serialized_result = json.dumps(
            self.aws.to_mapping(), default=lambda x: list(x) if isinstance(x, set) else x
        )
        path_to_expected = os.path.join(const.ASSETS_DIR, "expected-aws-provision-fail.json")
        with open(path_to_expected, encoding="utf-8") as expected_file:
            self.assertEqual(json.loads(serialized_result), json.load(expected_file))

    def test_is_provisioned(self):
        """Ensure is_provisioned calls get_provisioning_state that returns 2 item tuple."""
        with mock.patch.object(self.aws, 'get_provisioning_state', lambda *args: (True, None)):
            self.assertTrue(self.aws.is_provisioned([None]))

    @mock.patch.dict(os.environ, {
        'AWS_UPT_LAUNCH_TEMPLATE_NAME': 'template',
        'AWS_UPT_INSTANCE_PREFIX': const.AWS_UPT_INSTANCE_PREFIX,
        'CI_PIPELINE_ID': '123',
        'CI_JOB_ID': '456',
    })
    @mock.patch("time.sleep", mock.Mock())
    @mock.patch("upt.logger.LOGGER.printc")
    def test_provision_host(self, mock_printc):
        """Ensure provision_host works / is called with correct params."""
        job_id = os.environ["CI_JOB_ID"]
        host = next(iter(self.aws.hosts()))

        with (
            mock.patch.object(self.aws.ec2_client, "create_fleet") as mock_create_fleet,
            mock.patch.object(self.aws.ec2_resource, "Instance") as mock_instance,
        ):
            mock_create_fleet.side_effect = [
                # Fail twice
                {"Errors": [{"ErrorMessage": "Failed", "ErrorCode": "1"}]},
                {"Errors": [{"ErrorMessage": "Oh no", "ErrorCode": "2"}]},
                # And succeed
                {"Instances": [{"InstanceIds": ["i-12345"]}]},
            ]

            with (
                self.assertLogs(logger="root", level="WARNING") as log_root_ctx,
                self.assertLogs(logger=LOGGER, level="WARNING") as log_ctx,
            ):
                result = self.aws.provision_host(host)

            mock_printc.assert_called_with("Provisioned instance: i-12345", color=COLORS.GREEN)
            expected_logs = [
                f"WARNING:{LOGGER.name}:Failed to provision instance: ['Failed (ErrorCode=1)']",
                f"WARNING:{LOGGER.name}:Failed to provision instance: ['Oh no (ErrorCode=2)']",
            ]
            self.assertEqual(expected_logs, log_ctx.output)
            expected_root_logs = [
                f"WARNING:root:RETRY func({self.aws}, {host}), delaying for 3s",
                f"WARNING:root:RETRY func({self.aws}, {host}), delaying for 6s",
            ]
            self.assertEqual(expected_root_logs, log_root_ctx.output)

            self.assertEqual(result, mock_instance.return_value)
            mock_instance.assert_called_once_with("i-12345")

            instance_name = f"j{job_id}.r{host.recipe_id}.3"
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [],
                }],
                'OnDemandOptions': {},
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{instance_name}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': job_id}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    @mock.patch.dict(
        os.environ,
        {
            "AWS_UPT_LAUNCH_TEMPLATE_NAME": "template",
            "AWS_UPT_INSTANCE_PREFIX": const.AWS_UPT_INSTANCE_PREFIX,
            "CI_PIPELINE_ID": "123",
            "CI_JOB_ID": "456",
            "AWS_UPT_IMAGE_ID": "image",  # override image id
            "AWS_UPT_MAX_TOTAL_PRICE": "1.00",  # override max price
        },
    )
    @mock.patch("upt.logger.LOGGER.printc")
    def test_provision_host_kwarg_override(self, mock_printc):
        """Ensure provision_host correctly overrides kwargs if requested."""
        job_id = os.environ["CI_JOB_ID"]
        host = list(self.aws.hosts())[0]

        with mock.patch.object(self.aws.ec2_client, 'create_fleet') as mock_create_fleet:
            mock_create_fleet.return_value = {"Instances": [{"InstanceIds": ["i-12345"]}]}

            self.aws.provision_host(host)

            mock_printc.assert_called_with("Provisioned instance: i-12345", color=COLORS.GREEN)

            instance_name = f"j{job_id}.r{host.recipe_id}.1"
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [{'ImageId': 'image'}],
                }],
                'OnDemandOptions': {
                    'MaxTotalPrice': '1.00',
                },
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{instance_name}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': '456'}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    @mock.patch.dict(
        os.environ,
        {
            "AWS_UPT_LAUNCH_TEMPLATE_NAME": "template",
            "AWS_UPT_INSTANCE_PREFIX": const.AWS_UPT_INSTANCE_PREFIX,
            "CI_PIPELINE_ID": "123",
            "CI_JOB_ID": "456",
            "AWS_UPT_IMAGE_ID": "",  # empty override
        },
    )
    @mock.patch("upt.logger.LOGGER.printc")
    def test_provision_host_kwarg_override_empty(self, mock_printc):
        """Ensure provision_host correctly ignores empty overrides."""
        job_id = os.environ["CI_JOB_ID"]
        host = list(self.aws.hosts())[0]

        with mock.patch.object(self.aws.ec2_client, 'create_fleet') as mock_create_fleet:
            mock_create_fleet.return_value = {"Instances": [{"InstanceIds": ["i-12345"]}]}

            self.aws.provision_host(host)

            mock_printc.assert_called_with("Provisioned instance: i-12345", color=COLORS.GREEN)

            instance_name = f"j{job_id}.r{host.recipe_id}.1"
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [],
                }],
                'OnDemandOptions': {},
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{instance_name}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': '456'}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    @mock.patch.dict(
        os.environ,
        {
            "AWS_UPT_LAUNCH_TEMPLATE_NAME": "template",
            "AWS_UPT_INSTANCE_PREFIX": const.AWS_UPT_INSTANCE_PREFIX,
            "CI_PIPELINE_ID": "123",
            "CI_JOB_ID": "456",
            "AWS_UPT_NETWORK_SUBNET_IDS": "subnet-1 subnet-2",  # override subnets
            "AWS_UPT_IMAGE_ID": "image",
        },
    )
    @mock.patch("upt.logger.LOGGER.printc")
    def test_provision_host_kwarg_override_subnet_ids(self, mock_printc) -> None:
        """Ensure provision_host correctly overrides subnet ids if requested."""
        job_id = os.environ["CI_JOB_ID"]
        host = list(self.aws.hosts())[0]

        with mock.patch.object(self.aws.ec2_client, 'create_fleet') as mock_create_fleet:
            mock_create_fleet.return_value = {"Instances": [{"InstanceIds": ["i-12345"]}]}

            self.aws.provision_host(host)

            mock_printc.assert_called_with("Provisioned instance: i-12345", color=COLORS.GREEN)

            instance_name = f"j{job_id}.r{host.recipe_id}.1"
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [
                        {'ImageId': 'image', 'SubnetId': 'subnet-1'},
                        {'ImageId': 'image', 'SubnetId': 'subnet-2'},
                    ],
                }],
                'OnDemandOptions': {},
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{instance_name}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': '456'}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    @mock.patch.dict(
        os.environ,
        {
            "AWS_UPT_LAUNCH_TEMPLATE_NAME": "template",
            "AWS_UPT_INSTANCE_PREFIX": const.AWS_UPT_INSTANCE_PREFIX,
            "CI_PIPELINE_ID": "123",
            "CI_JOB_ID": "456",
        },
    )
    @mock.patch("upt.logger.LOGGER.printc")
    def test_reprovision_aborted(self, mock_printc):
        """Ensure reprovision_aborted works."""
        host2reprovision = list(self.aws.hosts())[0]
        host2reprovision.instance = mock.Mock(state={"Code": EC2_INSTANCE_RUNNING + 1})

        with mock.patch.object(self.aws.ec2_client, "create_fleet") as mock_create_fleet:
            mock_create_fleet.return_value = {"Instances": [{"InstanceIds": ["i-12345"]}]}
            self.aws.reprovision_aborted(self.aws.rgs[0])

            self.assertEqual(
                mock_printc.mock_calls,
                [
                    mock.call(
                        "Reprovisioning instance. Failed to reach i-00000000000000001",
                        color=COLORS.YELLOW,
                    ),
                    mock.call("Provisioned instance: i-12345", color=COLORS.GREEN),
                ],
            )
            mock_create_fleet.assert_called_once()
            self.assertEqual(host2reprovision.misc["instance_id"], "i-12345")
            self.assertEqual(len(self.aws.rgs[0].erred_rset_ids), 1)

    def test_no_reprovision_aborted(self):
        """Ensure reprovision_aborted isn't called when all hosts are in OK state."""
        host2reprovision = list(self.aws.hosts())[0]
        with mock.patch.object(host2reprovision, 'instance') as mock_inst:
            with mock.patch.object(self.aws, 'provision_host') as mock_provision_host:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
                self.aws.reprovision_aborted(self.aws.rgs[0])

                mock_provision_host.assert_not_called()
                self.assertEqual(len(self.aws.rgs[0].erred_rset_ids), 0)

    @mock.patch('subprocess.run')
    def test_get_provisioning_state(self, mock_run):
        """Ensure get_provisioning_state returns provisioned when instances are running."""
        rg2test = self.aws.rgs[0]
        mock_run.return_value.returncode = 0

        with mock.patch.object(self.aws.ec2_resource, 'Instance') as mock_inst:
            mock_inst.return_value.state = {'Code': EC2_INSTANCE_RUNNING}
            mock_inst.return_value.launch_time = misc.now_tz_utc()

            provisioned, erred = self.aws.get_provisioning_state(rg2test)
            self.assertEqual(erred, [])
            self.assertTrue(provisioned)

    @mock.patch('subprocess.run')
    def test_heartbeat(self, mock_run):
        """Ensure heartbeat works."""
        rg2test = self.aws.rgs[0]
        mock_run.return_value.returncode = 0

        with mock.patch.object(self.aws.ec2_resource, 'Instance') as mock_inst:
            mock_inst.return_value.state = {'Code': EC2_INSTANCE_RUNNING + 1}
            mock_inst.return_value.launch_time = misc.now_tz_utc()

            recipe_ids_dead = set()
            self.aws.heartbeat(rg2test, recipe_ids_dead)

            self.assertEqual(recipe_ids_dead, {1})

    def test_get_provisioning_state_provisioning_failed(self):
        """Ensure get_provisioning_state returns not provisioned on provisioning failures."""
        resource_group = self.aws.rgs[0]
        host = next(iter(resource_group.hosts()))
        host.misc = {"instance_id": None}

        with self.assertLogs(logger=LOGGER, level="DEBUG") as log_ctx:
            provisioned, erred = self.aws.get_provisioning_state(resource_group)

        expected_logs = f"DEBUG:{LOGGER.name}:Host not provisioned ({host.to_mapping()!r})"
        self.assertIn(expected_logs, log_ctx.output)

        self.assertEqual(erred, [host])
        self.assertFalse(provisioned)

    @mock.patch('upt.logger.LOGGER.debug')
    def test_get_provisioning_state_no_instance(self, mock_debug):
        """Ensure get_provisioning_state returns not provisioned for ClientError."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, 'instance') as mock_inst:
            mock_inst.id = 1
            client_error = ClientError(
                error_response={'Error': {'Code': 'InvalidInstanceID.NotFound'}},
                operation_name='DescribeInstances')
            mock_inst.reload.side_effect = itertools.chain([client_error], itertools.cycle([None]))

            provisioned, erred = self.aws.get_provisioning_state(self.aws.rgs[0])
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)
            mock_debug.assert_called_with('Waiting for host %s to be created', mock_inst.id)

    def test_get_provisioning_state_no_dns(self):
        """Ensure get_provisioning_state returns not provisioned without public dns name."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, 'instance') as mock_inst:
            mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
            mock_inst.launch_time = misc.now_tz_utc()
            mock_inst.public_dns_name = ''

            provisioned, erred = self.aws.get_provisioning_state(self.aws.rgs[0])
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)
            mock_inst.reload.assert_called()

    @mock.patch('subprocess.run')
    def test_get_provisioning_state_no_user_data(self, mock_run):
        """Ensure get_provisioning_state doesn't call ssh readiness after user script."""

        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'reachable_via_ssh', True):
            with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'instance') as mock_inst:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
                mock_inst.launch_time = misc.now_tz_utc()
                self.aws.get_provisioning_state(self.aws.rgs[0])
                mock_run.assert_not_called()

    @mock.patch('subprocess.run')
    @mock.patch('upt.logger.LOGGER.debug')
    @freeze_time("2025-02-21 10:30:00+00:00")
    def test_get_provisioning_state_ssh(self, mock_debug, mock_run):
        """Ensure get_provisioning_state uses ssh readiness check when user script isn't done."""
        rg2test = self.aws.rgs[0]
        mock_run.return_value.stdout = 'some output'
        mock_run.return_value.stderr = 'some error'
        mock_run.return_value.returncode = 1

        with (
            self.subTest("Logs SSH outcome"),
            mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], "instance") as mock_inst,
        ):
            mock_inst.id = 1
            mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
            mock_inst.launch_time = misc.now_tz_utc() - misc.parse_timedelta("15m")

            provisioned, erred = self.aws.get_provisioning_state(rg2test)
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)

            mock_debug.assert_any_call(mock.ANY, 'some output')
            mock_debug.assert_any_call(mock.ANY, 'some error')
            mock_debug.assert_any_call(
                'Waiting for host %s to finish UserData script', mock_inst.id)

        with (
            self.subTest("Halts on timeout"),
            mock.patch.object(rg2test.recipeset.hosts[0], "instance") as mock_inst,
        ):
            mock_inst.id = 1
            mock_inst.state = {"Code": EC2_INSTANCE_RUNNING}
            mock_inst.launch_time = misc.now_tz_utc() - misc.parse_timedelta("15m 1s")

            provisioned, erred = self.aws.get_provisioning_state(rg2test)
            self.assertEqual(erred, [rg2test.recipeset.hosts[0]])
            self.assertFalse(provisioned)

            mock_inst.terminate.assert_called_once_with()
            mock_debug.assert_any_call("Host %s failed to become available for ssh", mock_inst.id)

    def test_get_resource_ids(self):
        """Ensure test_get_resource_ids returns instance ids."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, 'misc', {'instance_id': 'i-1234'}):
            self.assertEqual(['i-1234'], self.aws.get_resource_ids())
