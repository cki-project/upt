"""Test cases for upt __main__ module."""
import unittest
from unittest import mock

from tests.utils import create_temporary_files
from tests.utils import create_temporary_kcidb
from upt.__main__ import main
from upt.logger import COLORS
from upt.misc import RET


class TestUPT(unittest.TestCase):
    """Test cases for upt __main__ module."""

    def setUp(self):
        self.mock_logger_add_fhandler = mock.patch('upt.__main__.logger_add_fhandler', mock.Mock())
        self.mock_logger_add_fhandler.start()

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    @mock.patch('upt.cmd_provision.ProvisionerGlue.run_provisioners')
    def test_provision_api_main(self, mock_run):
        """Test that main provision works."""
        with (
                create_temporary_kcidb() as kcidb_path,
                create_temporary_files(['ignore']),
                mock.patch('sys.argv',
                           ['__main__.py', '--kcidb-file', kcidb_path,
                            'provision', '-r', 'ignore'])
        ):
            main()
            mock_run.assert_called()

    @mock.patch("upt.logger.LOGGER.printc")
    def test_provision_api_main_dry_run(self, mock_printc):
        """Test that main provision works."""
        with (
                create_temporary_kcidb() as kcidb_path,
                create_temporary_files(['ignore']),
                mock.patch('sys.argv',
                           ['__main__.py', '--kcidb-file', kcidb_path,
                            'provision', '-r', 'ignore', '--dry-run'])
        ):
            main()

        mock_printc.assert_called_with(
            'dry run: skipping provisioning resource(s)', color=COLORS.MAGENTA
        )

    @mock.patch('upt.__main__.sentry_init')
    def test_main_sentry_init(self, sentry_init_mock):
        """Test main calls sentry_init."""
        with (
                create_temporary_kcidb() as kcidb_path,
                create_temporary_files(['ignore']),
                mock.patch('sys.argv',
                           ['__main__.py', '--kcidb-file', kcidb_path,
                            'provision', '-r', 'ignore'])
        ):
            main()
            assert sentry_init_mock.called

    @mock.patch('builtins.print')
    @mock.patch('upt.glue.ProvisionerGlue.do_wait')
    def test_waiton_api(self, mock_do_wait, mock_print):
        """Test that waiton works."""
        mock_do_wait.return_value = RET.PROVISIONING_PASSED

        with (
                create_temporary_kcidb() as kcidb_path,
                create_temporary_files(['ignore']),
                mock.patch('sys.argv',
                           ['__main__.py', '--kcidb-file', kcidb_path, 'waiton', '-r', 'ignore'])
        ):
            main()
            mock_print.assert_any_call('Waiting on existing provisioning request...')
