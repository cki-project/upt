"""Test for upt.smoke_test.render module."""
from io import StringIO
from pathlib import Path
import tempfile
import unittest
from unittest.mock import patch
import xml.etree.ElementTree as ET

from tests.const import ASSETS_DIR
from upt.smoke_test import render


class TestRender(unittest.TestCase):
    """Test template rendering."""

    maxDiff = None

    def check_recipesets(self, xml_content: str, number_of_recipesets: int) -> None:
        """Helper method."""
        tree = ET.fromstring(xml_content)
        self.assertEqual(number_of_recipesets, len(tree.findall('.//recipeSet')))

    def setUp(self):
        """Common stuff."""
        self.template_file = 'smoke_test.j2'
        self.default_smoke_tests = {
            'smoke_tests': render.SINGLE_SMOKE_TESTS,
            'arch': 'x86_64',
            'distro': 'CentOSStream9',
        }

        self.warn_smoke_test = {
            'smoke_tests': ['warn'],
            'arch': 'x86_64',
            'distro': 'CentOSStream9',
        }

        self.all_smoke_tests = {
            'smoke_tests': render.get_smoke_tests(['all']),
            'arch': 'x86_64',
            'distro': 'CentOSStream9',
        }

    def test_get_smoke_tests(self):
        """Test get_smoke_tests."""
        cases = (
            ('Without smoke tests, it returns the default smoke_tests',
             None, render.SINGLE_SMOKE_TESTS),
            ('Cleaning up some repeated elements',
             ['fail', 'fail'], ['fail']),
            ('Expanding groups', ['multihosts'], render.MULTIHOSTS_SMOKE_TESTS),
            ('Cleaning and expanding with repetitions', ['fail', 'multihosts'],
             ['fail'] + render.MULTIHOSTS_SMOKE_TESTS),
            ('Cleaning and expanding with repetitions',
             ['multihosts_abort_fail', 'multihosts_abort_fail', 'multihosts', 'multihosts'],
             render.MULTIHOSTS_SMOKE_TESTS)
        )
        for description, initial_smoke_tests, expected in cases:
            with self.subTest(description):
                self.assertCountEqual(expected, render.get_smoke_tests(initial_smoke_tests))

    def test_get_template_variables(self):
        """Test get template variables."""
        cases = (
            (
                'With default values',
                {
                    'arch': 'x86_64',
                    'distro': 'CentOSStream9',
                    'smoke_tests': None
                },
                {
                    'arch': 'x86_64',
                    'distro': 'CentOSStream9',
                    'smoke_tests': render.SINGLE_SMOKE_TESTS
                }
            ),
            (
                'With custom values',
                {
                    'arch': 'aarch64',
                    'distro': 'RedHatEnterpriseLinux7',
                    'smoke_tests': ['abort_recipe', 'fail']
                },
                {
                    'arch': 'aarch64',
                    'distro': 'RedHatEnterpriseLinux7',
                    'smoke_tests': ['abort_recipe', 'fail']
                }
            ),
        )
        for description, parsed_vars, expected in cases:
            with self.subTest(description):
                args = render.get_template_variables(parsed_vars)
                self.assertEqual(args['arch'], expected['arch'])
                self.assertEqual(args['distro'], expected['distro'])
                self.assertCountEqual(args['smoke_tests'], expected['smoke_tests'])

    def test_render_function(self):
        """Test render function."""

        cases = (
            ('Default smoke_tests', self.template_file, self.default_smoke_tests),
            ('One smoke_test', self.template_file, self.warn_smoke_test),
        )
        for description, template_file, template_variables in cases:
            with self.subTest(description):
                output = render.render(template_file, template_variables)
                self.check_recipesets(output, len(template_variables['smoke_tests']))

    def test_main_using_stdout(self):
        """Test main function."""
        cases = (
            ('With default values', [], len(render.SINGLE_SMOKE_TESTS)),
            ('Selecting smoke tests', ['--smoke-test', 'warn'], 1),
        )
        for description, args, expected in cases:
            with self.subTest(description):
                with patch('sys.stdout', new=StringIO()) as output:
                    render.main(args)
                    self.check_recipesets(output.getvalue(), expected)

    def test_main_using_files(self):
        """Test main function."""
        output_file = 'beaker.xml'
        cases = (
            {
                'description': 'With default values',
                'args': ['--output', output_file],
                'expected': len(render.SINGLE_SMOKE_TESTS)
            },
            {
                'description': 'Selecting smoke tests',
                'args': ['--smoke-test', 'warn', '--output', output_file],
                'expected': 1
            }
        )
        for case in cases:
            with self.subTest(case['description']):
                with tempfile.TemporaryDirectory():
                    render.main(case['args'])
                    xml_content = Path(output_file).read_text(encoding='utf-8')
                    self.check_recipesets(xml_content, case['expected'])

    def test_check_output(self):
        """Check output."""
        assest_content = Path(ASSETS_DIR, 'beaker_smoke_test.xml').read_text(encoding='utf-8')
        xml_content = render.render(self.template_file, self.all_smoke_tests)
        self.assertEqual(assest_content, xml_content)
        self.assertEqual(ET.canonicalize(assest_content), ET.canonicalize(xml_content))
