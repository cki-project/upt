# UPT

Unified Provisioning Tool

## Description

UPT is a Python-3 project for provisioning and testing in CKI Pipelines.

Beaker is quite closely tied with restraint. To run restraint, one needs to
know what tests to run, a "fake recipe id" and a hostname. The first two are no
issue. The fake recipe id is used to determine what host (identified by this
recipe id) will run what tests. However, with Beaker, we don't necessarily know
right away what hostname will be provisioned. Because of this, UPT waits until
the resource is provisioned and only then it can output restraint commands to
run tests.

Currently, a wrapper / test runner around `restraint` is part of this project.
It uses restraint standalone mode to run tests on a provisioned resource(s) We
realize it may need to be moved away. The design of the modules is made with
that in mind.

## Installation & dependencies

* The project needs `bkr` and `restraint` executables. Install the packages by:

```bash
dnf install -y beaker-client restraint-client`
```

* Optional: ensure your pip and setuptools are up-to-date:

```bash
pip3 install setuptools --upgrade --user
pip3 install pip --upgrade --user
```

* Install Python libraries & UPT:

```bash
cd upt && pip3 install --user --editable .`
```

* (`upt` and `rcclient` links are created automatically)

## Provisioners status

* Beaker:
  * full support: consumes any Beaker XML job to provide provisioning and
    test running
* AWS:
  * machine configuration given by launch template, with limited override
    capabilities

You can consider provisioning a machine without UPT and using just the test
runner. Refer to the YAML input format further below.

## Usage and provisioner format

### Basic usage

* Drop `beaker.xml` file to `upt` directory.
* Run `upt legacy convert`. This will convert `beaker.xml` to `c_req.yaml`.
* Run `upt provision` with any extra options you may need.

If you wish to wait for all provisioning to complete before running tests, you
can wait for the provisioning to complete and run `rcclient test` afterwards.

If you wish to run testing right away as resources are available
(asynchronously from other resources in the request), you can use `--pipeline
"<rcclient options>"` with the `upt provision` command.

### Basic usage explained + provisioner format

Below is an example yaml file content (provisioning request) to provision 1
job/recipe/host and run 1 task.

```xml
!<ProvisionData>
provisioners:
  - !<AWS>
    rgs:
      - !<ResourceGroup>
        resource_id: J:1234
        ssh_opts: ''
        recipeset: !<RecipeSet>
          hosts:
            - !<Host>
              hostname: host_to_run_on
              recipe_id: 1
              recipe_fill: |-
                <recipe> ... </recipe>
              duration: 2880
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job id="1">
             <recipeSet>
              <recipe id="1" job_id="1">
               <task name="/distribution/command">
                <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz#command"/>
                <params>
                 <param name="CKI_NAME" value="test1"/>
                 <param name="CMDS_TO_RUN" value="exit 0"/>
                </params>
              </recipe>
             </recipeSet>
            </job>
        job: ''
    name: beaker
```

Here are some basic rules:

* A file may use/contain multiple provisioners.
* Each `ResourceGroup` object has 1 `RecipeSet`.
* There can be multiple `ResourceGroup` objects in the file.
* Each `RecipeSet` may have multiple hosts.
* Each `recipe_fill` explains how to provision one (1) host.
* Each `restraint_xml` explains what tests to run for one (1) `RecipeSet`.
* `recipeid` / `hostname` fields explicitly tell what tests to run on what
  host. See `id` attribute of a recipe in `resraint_xml`.
* Beaker only: `duration` specifies for how long (in seconds) will the host
  stay provisioned. Each time we start running tests on the host, we renew this
  duration, because we certainly don't want the machine to disappear in the
  middle of testing.

The `!<text>` labels are yaml labels for Python classes, so yaml library
automatically knows how to deserialize into objects.

Running `upt --help` or `rcclient --help` is your friend. The parameters should
be reasonably documented there. If not, then let's fix that.

### AWS provisioner

To use the AWS provisioner, you need to have an AWS account and your system has to
be configured to use it. This usually includes setting AWS access key id, AWS
secret access key and AWS region in config file. Refer to
<https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html>
for more info.

#### General setup

| Name                           | Secret | Required | Description                                                    |
|--------------------------------|--------|----------|----------------------------------------------------------------|
| `AWS_UPT_ACCESS_KEY_ID`        | no     | no       | Access key of the AWS service account used to create instances |
| `AWS_UPT_SECRET_ACCESS_KEY`    | yes    | no       | Secret key of the AWS service account used to create instances |
| `AWS_UPT_INSTANCE_PREFIX`      | no     | yes      | Prefix for the EC2 instance name                               |
| `AWS_UPT_LAUNCH_TEMPLATE_NAME` | no     | yes      | Name of the launch template to use for creating instances      |
| `AWS_UPT_IMAGE_ID`             | no     | no       | Override the image ID in the launch template                   |
| `AWS_UPT_NETWORK_SUBNET_IDS`   | no     | no       | Override the subnets from the template                         |
| `AWS_UPT_MAX_TOTAL_PRICE`      | no     | no       | Maximum price per hour                                         |
| `AWS_UPT_TIMEOUT`              | no     | no       | Timedelta before giving up on provisioning. Defaults to 15m.   |

#### Details

Otherwise, using AWS provisioner is very similar to using Beaker. The biggest
difference is that the node is named `aws`.

When an instance is provisioned, `misc` field and `instance_id` is created in
`Host` node. This allows UPT to use existing running instances using `upt
waiton` command, instead of provisioning them. This is useful especially during
development; you don't have to keep launching and terminating instances.

Notes:

* Currently, the provisioner provisions only `t2.micro` instances (hardcoded).
* `duration` and `resource_id` fields are currently ignored.

```xml
!<ProvisionData>
provisioners:
  - !<AWS>
    rgs:
      - !<ResourceGroup>
        resource_id: '1'
        ssh_opts: ''
        recipeset: !<RecipeSet>
          hosts:
            - !<Host>
              hostname: hostname
              recipe_id: 1
              recipe_fill:
              duration: 28800
              misc:
                instance_id: i-00000000000000001
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job id="1">
             <recipeSet>
              <recipe id="1" >
               <task name="/distribution/command">
                <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz#command"/>
                <params>
                 <param name="CKI_NAME" value="test1"/>
                 <param name="CMDS_TO_RUN" value="exit 0"/>
                </params>
               </task>
             </recipeSet>
            </job>
          id:
        job: <job group retention_tag="60days"><whiteboard>UPT@gitlab:123456 3.18@rhel x86_64</whiteboard></job>
    name: aws
```

### Testing Farm (TF) provisioner

To be able to use Testing Farm you need an API key. The authentication is expected to
change with API version updates, follow the steps according to API version you are using.
<https://docs.testing-farm.io/Testing%20Farm/0.1/onboarding.html#_api_version_v0_1>
for more info.

#### TF General setup

| Name                            | Secret | Required | Description                                           |
|---------------------------------|--------|----------|-------------------------------------------------------|
| `TF_API_TOKEN`                  | yes    | yes      | Access key of the TF account used to send requests    |
| `TF_API_URL`                    | no     | yes      | Testing Farm API URL for submitting requests          |
| `OS`                            | no     | no       | OS to be used for provisioning requests               |
| `TF_POOL`                       | no     | no       | Used if passing TF dedicated pool for provisioning    |
| `arch`                          | no     | yes      | Architecture to be used for provisioning requests     |
| `AUTOMOTIVE_TOOLCHAIN_URL`      | yes    | no       | This is where automotive image compose can be located |
| `AUTOMOTIVE_CONFIGURATION_PATH` | no     | no       | Local pipeline json which contains image information  |
| `AUTOMOTIVE_RELEASE`            | no     | no       | If testing automotive, which release should be tested |

#### TF Details

Either OS or automotive compose information must be defined. Both can not be null. If the request
is for testing automotive hardware, all related compose information is required to be provided.

When an instance is provisioned the `misc` and `resource_id` entries are created in the
`Host` node. The successful TF request will return the guest id which will be assigned to this field.
Upt will then poll testing farm for the status of the request. Once poling the provisioning logs identifies
a host ip, that value is assigned to the `hostname` field.

Notes:

* `duration` field is currently ignored and is set to 1440 minutes

```xml
!<ProvisionData>
provisioners:
  - !<TestingFarm>
    rgs:
      - !<ResourceGroup>
        resource_id: '1'
        ssh_opts: ''
        recipeset: !<RecipeSet>
          hosts:
            - !<Host>
              hostname: hostname
              recipe_id: 1
              recipe_fill:
              duration: 28800
              misc:
                resource_id:
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job id="1">
             <recipeSet>
              <recipe id="1" >
               <task name="/distribution/command">
                <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz#command"/>
                <params>
                 <param name="CKI_NAME" value="test1"/>
                 <param name="CMDS_TO_RUN" value="exit 0"/>
                </params>
               </task>
             </recipeSet>
            </job>
          id:
        job: <job group retention_tag="60days"><whiteboard>UPT@gitlab:123456 3.18@rhel x86_64</whiteboard></job>
    name: testingfarm
```

## Testing UPT with beaker

UPT is mainly used to run tests against beaker. To generate those jobs, we don't want to use real tests,
we want to use smoke tests stored at [kernel-tests](https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/tree/main/upt_smoke_tests).

We always will select virtual machines instead of bare-metal to avoid blocking real jobs.

To generate those beaker files, you can use our smoke test render

```bash
usage: render.py [-h]
                 [--smoke-test {abort_recipe,abort_task,fail,kernel_panic,skip,pass,warn,multihosts_abort_recipe,multihosts_abort_fail,multihosts_panic,multihosts_skip,multihosts_fail,multihosts_warn,all,single,multihosts}]
                 [--arch {aarch64,s390x,ppc64le,x86_64}]
                 [--distro {CentOSStream9,Fedora35,Fedora36,Fedora37,FedoraELN,Fedorarawhide,RedHatEnterpriseLinux7,RedHatEnterpriseLinux8,RedHatEnterpriseLinux9}]
                 [--output OUTPUT]

Render Beaker XML for upt smoke tests

optional arguments:
  -h, --help            show this help message and exit
  --smoke-test {abort_recipe,abort_task,fail,kernel_panic,skip,pass,warn,multihosts_abort_recipe,multihosts_abort_fail,multihosts_panic,multihosts_skip,multihosts_fail,multihosts_warn,all,single,multihosts}
                        Select smoke test. Defaults single smoke tests ['abort_recipe', 'abort_task', 'fail', 'kernel_panic', 'skip', 'pass', 'warn']
  --arch {aarch64,s390x,ppc64le,x86_64}
                        Select the architecture. Defaults to x86_64
  --distro {CentOSStream9,Fedora35,Fedora36,Fedora37,FedoraELN,Fedorarawhide,RedHatEnterpriseLinux7,RedHatEnterpriseLinux8,RedHatEnterpriseLinux9}
                        Select the distro. Defaults to CentOSStream9
  --output OUTPUT       Output path for the rendered file. Defaults STDOUT
```

For example, if you want to generate a beaker file for `aarch64` with `pass` and `warn` smoke tests,
you should type

```bash
python3 -m upt.smoke_test.render --smoke-test pass --smoke-test warn --arch aarch64 --output smoke_test.xml
```

We have three different groups of smoke tests to be easier to test:

* `all`: All smoke tests (single and multi hosts).
* `single`: All single smoke tests.
* `multihosts`: All multi hosts tests.

### Notes

Currently, machine has 15 minutes to reboot. If it's longer, it's considered an
issue.

### Even more info

See `docs` directory, it contains useful info about some design decisions and
explains why UPT works the way it does.
