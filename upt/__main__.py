#!/usr/bin/env python3
"""Main entry point, command-line interface for upt."""
import argparse
import os
import sys

from cki_lib.misc import sentry_init
import sentry_sdk

from upt.logger import LOGGER
from upt.logger import logger_add_fhandler

from . import cmd_legacy
from . import cmd_provision
from . import cmd_waiton
from . import misc


def create_parser():
    """Create the main parser."""
    description = "UPT - Unified Provisioning Tool"
    parser = argparse.ArgumentParser(description=description, add_help=False)
    parser.add_argument('-h', '--help', default=True, nargs='?',
                        const=True, help='Show this help and exit')
    parser.add_argument('--time-cap', default=0, type=int,
                        help='Specifies a time limit in minutes to finish provisioning. If the'
                        ' provisioner fails to provision within this time limit,'
                        ' then all resource(s) will be returned (e.g. Beaker job is'
                        ' canceled). Default: 0 (disabled).')
    parser.add_argument('--max-aborted-count', default=3, type=int,
                        help='Specifies the number of attempts to provision a resource.'
                        'Default: 3.')
    parser.add_argument('--force-host-duration', default=None, type=int,
                        help='If used, forces a static duration for how long the '
                        'resource is provisioned. If not used, we add-up '
                        'KILLTIMEOVERRIDE values of tasks in each recipe.')
    parser.add_argument('--kcidb-file', help='Path to KCIDB file with run data')
    parser.add_argument('-e', '--excluded-hosts', default='',
                        help='A path to a file with newline separated hostnames'
                        ' that we do not want to run tests on.')
    parser.add_argument('--keycheck', default=False, action=argparse.BooleanOptionalAction,
                        help='Do strict ssh host keychecking.')
    parser.add_argument('--log-dir', default=os.environ.get('UPT_OUTPUT_DIR', os.getcwd()),
                        help='Output directory for the logfile. Default: specified'
                        ' by UPT_OUTPUT_DIR environment variable, '
                        'otherwise current directory.')
    return parser


def main():
    """Do everything; main entrypoint."""
    sentry_init(sentry_sdk)
    common_parser = argparse.ArgumentParser(add_help=False)
    parser = create_parser()
    cmds_parser = parser.add_subparsers(title="Command", dest="command")
    cmd_legacy.build(cmds_parser, common_parser)
    cmd_provision.build(cmds_parser, common_parser)
    cmd_waiton.build(cmds_parser, common_parser)

    args = parser.parse_args()
    # Put log file info.log into current directory.
    logger_add_fhandler(LOGGER, 'upt_info.log', args.log_dir)
    misc.setup_args(args)

    commands = {
        'help': [parser.print_help],
        'legacy': [cmd_legacy.main, args],
        'provision': [cmd_provision.main, args],
        'waiton': [cmd_waiton.main, args],
    }

    command_name = args.command if args.command is not None else "help"
    function, *function_args = commands[command_name]
    try:
        function(*function_args)
    except misc.ActionNotFound as exc:
        print(f'{exc}', file=sys.stderr)
        cmds_parser.choices[args.command].print_help(file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main()
