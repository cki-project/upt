"""AWS provisioner."""
from functools import cached_property
import os
import subprocess

import boto3
import botocore.exceptions
from cki_lib import misc
from cki_lib.retrying import retrying_on_exception
from ruamel.yaml.scalarstring import PreservedScalarString

from upt.const import EC2_INSTANCE_RUNNING
from upt.logger import COLORS
from upt.logger import LOGGER
from upt.misc import Monotonic
from upt.misc import fixup_or_delete_tasks_without_fetch
from upt.plumbing.interface import ProvisionerCore
from upt.plumbing.objects import Host
from upt.restraint.file import RestraintJob


class AWSProvisionError(Exception):
    """Failed to provision an instance in AWS."""


class AWS(ProvisionerCore):
    """AWS provisioner."""

    session = boto3.Session(aws_access_key_id=os.environ.get('AWS_UPT_ACCESS_KEY_ID'),
                            aws_secret_access_key=os.environ.get('AWS_UPT_SECRET_ACCESS_KEY'))
    aws_upt_timeout = misc.parse_timedelta(os.environ.get("AWS_UPT_TIMEOUT", "15m"))

    def __init__(self, dict_data=None):
        """Initialize AWS provisioner."""
        super().__init__(dict_data)
        self.provision_host_monotonic = Monotonic()

    @cached_property
    def ec2_resource(self):
        # pylint: disable=no-self-use
        """Get AWS's session ec2."""
        return AWS.session.resource('ec2')

    @cached_property
    def ec2_client(self):
        # pylint: disable=no-self-use
        """Get AWS's session ec2."""
        return AWS.session.client('ec2')

    def hosts(self):
        """Iterate through all AWS hosts."""
        for resource_group in self.rgs:
            yield from resource_group.hosts()

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        self.keycheck = kwargs.get('keycheck')

        for resource_group in self.rgs:
            # We currently don't have a use for resource_id with AWS, so just
            # use an increasing unique number.
            resource_group.resource_id = str(self.resource_id_monotonic.get())
            recipe_set = resource_group.recipeset
            restraint_job = RestraintJob.create_from_string(recipe_set.restraint_xml)
            restraint_recipes = restraint_job.get_all_recipes()

            assert len(restraint_recipes) == len(
                recipe_set.hosts), "Invalid data; must have 1 host per 1 recipe"

            for j, host in enumerate(recipe_set.hosts):
                instance_id = host.misc.get('instance_id')
                if not instance_id:
                    host.recipe_id = self.host_recipe_id_monotonic.get()
                    try:
                        host.instance = self.provision_host(host)
                    except AWSProvisionError as error:
                        LOGGER.error(repr(error))
                else:
                    LOGGER.debug("Reusing provisioned instance: %s", instance_id)
                    host.instance = self.ec2_resource.Instance(instance_id)
                    host.misc["instance_id"] = host.instance.id

                restraint_recipes[j].id = str(host.recipe_id)

            fixup_or_delete_tasks_without_fetch(restraint_job)
            recipe_set.restraint_xml = PreservedScalarString(str(restraint_job))

    @retrying_on_exception(AWSProvisionError)
    def provision_host(self, host: Host):
        """Provision a single host."""
        ci_job_id = os.environ.get('CI_JOB_ID', '')
        instance_name = f"j{ci_job_id}.r{host.recipe_id}.{self.provision_host_monotonic.get()}"
        kwargs = {
            'LaunchTemplateConfigs': [{
                'LaunchTemplateSpecification': {
                    'LaunchTemplateName': os.environ['AWS_UPT_LAUNCH_TEMPLATE_NAME'],
                    'Version': '$Default',
                },
                'Overrides': [
                    {'SubnetId': subnet_id}
                    for subnet_id in os.environ.get('AWS_UPT_NETWORK_SUBNET_IDS', '').split()
                ],
            }],
            'OnDemandOptions': {},
            'TagSpecifications': [
                {
                    'ResourceType': 'instance',
                    'Tags': [
                        {'Key': 'Name',
                         'Value': f'{os.environ["AWS_UPT_INSTANCE_PREFIX"]}.{instance_name}'},
                        {'Key': 'CkiGitLabPipelineId',
                         'Value': os.environ.get('CI_PIPELINE_ID', '')},
                        {'Key': 'CkiGitLabJobId',
                         'Value': ci_job_id},
                    ]
                }
            ],
            'TargetCapacitySpecification': {
                'DefaultTargetCapacityType': 'on-demand',
                'TotalTargetCapacity': 1,
            },
            'Type': 'instant',
        }

        if image_id := os.environ.get('AWS_UPT_IMAGE_ID'):
            if not kwargs['LaunchTemplateConfigs'][0]['Overrides']:
                kwargs['LaunchTemplateConfigs'][0]['Overrides'] = [{}]
            for override in kwargs['LaunchTemplateConfigs'][0]['Overrides']:
                override['ImageId'] = image_id
        if max_total_price := os.environ.get('AWS_UPT_MAX_TOTAL_PRICE'):
            kwargs['OnDemandOptions']['MaxTotalPrice'] = max_total_price

        LOGGER.debug('Requesting instance via CreateFleet: %s', kwargs)
        response = self.ec2_client.create_fleet(**kwargs)
        LOGGER.debug('AWS response: %s', response)
        if not (instance_id := misc.get_nested_key(response, "Instances/0/InstanceIds/0")):
            formatted_errors = [
                f"{error.get('ErrorMessage')} (ErrorCode={error.get('ErrorCode')})"
                for error in response.get("Errors", [])
            ]

            LOGGER.warning("Failed to provision instance: %s", formatted_errors)
            raise AWSProvisionError(f"Failed to provision instance: {formatted_errors}")
        LOGGER.printc(f"Provisioned instance: {instance_id}", color=COLORS.GREEN)
        host.misc["instance_id"] = instance_id
        return self.ec2_resource.Instance(instance_id)

    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""
        provisioned, _ = self.get_provisioning_state(resource_group)
        return provisioned

    def get_provisioning_state(self, resource_group):
        """
        Get current state of provisioning.

        NOTE: this method is not a required part of the provisioner interface.
        Each provisioner implements and uses this method in its own way,
        although the method signature is very similar each time.
        """
        provisioned = True
        erred = []

        for host in resource_group.hosts():
            if not (instance_id := host.misc.get("instance_id")):
                provisioned = False
                LOGGER.debug("Host not provisioned (%r)", host.to_mapping())
                erred.append(host)
                continue

            if not host.instance:
                host.instance = self.ec2_resource.Instance(instance_id)
            try:
                host.instance.reload()
            # should be resolved by eventual consistency
            except botocore.exceptions.ClientError:
                provisioned = False
                LOGGER.debug('Waiting for host %s to be created', host.instance.id)
                continue

            if host.instance.state['Code'] < EC2_INSTANCE_RUNNING:
                provisioned = False
                LOGGER.debug('Waiting for host %s to reach running state', host.instance.id)
                continue

            if host.instance.state['Code'] > EC2_INSTANCE_RUNNING:
                provisioned = False
                LOGGER.debug('Host %s failed to read running state', host.instance.id)
                erred.append(host)
                continue

            # check if machine is reachable via ssh
            if not host.reachable_via_ssh:
                if misc.now_tz_utc() > host.instance.launch_time + self.aws_upt_timeout:
                    with misc.only_log_exceptions():
                        host.instance.terminate()
                    provisioned = False
                    LOGGER.debug('Host %s failed to become available for ssh', host.instance.id)
                    erred.append(host)
                    continue

                process = subprocess.run([
                    'ssh',
                    '-o', 'PubkeyAcceptedKeyTypes=+ssh-rsa',
                    '-o', f'StrictHostKeyChecking={self.keycheck}',
                    f'root@{host.instance.public_dns_name or host.instance.private_ip_address}',
                    '[', '$(whoami)', '=', 'root', ']'
                ], capture_output=True, check=False)
                if process.stderr.strip():
                    LOGGER.debug("stderr: %s", process.stderr)
                if process.stdout.strip():
                    LOGGER.debug("stdout: %s", process.stdout)
                if process.returncode:
                    LOGGER.debug('Waiting for host %s to finish UserData script', host.instance.id)
                    provisioned = False
                else:
                    host.reachable_via_ssh = True

        return provisioned, erred

    def reprovision_aborted(self, resource_group):
        """Provision a resource again, if provisioning failed."""
        for host in resource_group.hosts():
            if host.instance.state['Code'] > EC2_INSTANCE_RUNNING:
                LOGGER.printc(
                    f"Reprovisioning instance. Failed to reach {host.misc.get('instance_id')}",
                    color=COLORS.YELLOW,
                )
                # Count bad instance id as an aborted recipe set id
                resource_group.erred_rset_ids.add(host.misc.get('instance_id'))
                host.instance.terminate()
                del host.instance
                host.reachable_via_ssh = False
                # We just removed an instance that is somehow broken
                # (hopefully equivalent of Beaker "aborted"), so we will provision it again.
                host.instance = self.provision_host(host)
            else:
                LOGGER.debug(
                    "Skipping reprovision on instance that is not running: %s",
                    host.misc.get("instance_id"),
                )

    def release_rg(self, resource_group):
        """Release all resources provisioned in a single resource group."""
        for host in resource_group.hosts():
            if not host.instance:
                host.instance = self.ec2_resource.Instance(host.misc.get('instance_id'))
            host.instance.terminate()

    def heartbeat(self, resource_group, recipe_ids_dead):
        """
        Check if resource is OK (provisioned, not broken/aborted).

        Add all not OK recipe_id's to recipe_ids_dead
        """
        _, erred = self.get_provisioning_state(resource_group)
        # no op if empty set
        recipe_ids_dead |= {host.recipe_id for host in erred}
        # AWS has no concept of EWD, however, if the host becomes unresponsive,
        # we can still apply the same actions to it as if it suffered EWD.

    def update_provisioning_request(self, resource_group):
        """Ensure that request file has up-to-date info after provisioning.

        This method is to be called at the end, when we know we've successfully
        provisioned resources we need. This method will skip resources that
        failed provisioning.
        """
        for host in resource_group.hosts():
            host.hostname = host.instance.public_dns_name or host.instance.private_ip_address

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        return [host.misc.get('instance_id') for host in self.hosts()
                if host.misc.get('instance_id')]
