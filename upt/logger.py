"""Output loggers."""
import logging
import os

from cki_lib import logger


class COLORS:
    # pylint: disable=too-few-public-methods
    """Escape codes to colarize terminal output."""

    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'


def logger_add_fhandler(flogger: logging.Logger,
                        dst_file: str,
                        path: str) -> None:
    """Add filehandler to existing logger."""
    os.makedirs(path, exist_ok=True)
    dst_file = os.path.join(path, dst_file)

    fhandler = logging.FileHandler(dst_file)
    fhandler.setLevel(logging.DEBUG)
    flogger.addHandler(fhandler)


def file_logger(logger_name: str) -> logging.Logger:
    """Get CKI logger and add stream and file handlers to it."""
    flogger = logger.get_logger(logger_name)

    # Would probably be better to let get_logger handle propagation and
    # logging level, but can't both log DEBUG to file handler and defer to
    # root logger
    flogger.propagate = False
    flogger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(logger.STREAM)
    flogger.addHandler(handler)
    formatter = logging.Formatter(logger.FORMAT)
    handler.setFormatter(formatter)
    handler.setLevel(os.environ.get('CKI_LOGGING_LEVEL', 'WARNING'))

    return flogger


def colorize(data):
    """Colorize specific keywords using pre-defined colors."""
    color_table = {COLORS.YELLOW: ['Warn', 'Aborted', 'watchdog!'],
                   COLORS.GREEN: ['Pass', 'Running', 'done'],
                   COLORS.MAGENTA: ['Skip'],
                   COLORS.RED: ['FAIL', 'ERROR', 'Panic']}
    for word in data.split(' '):
        for color, keywords in color_table.items():
            for keyword in keywords:
                if keyword.lower() == word.lower():
                    data = data.replace(word, f'{color}{word}{COLORS.RESET}', 1)
                    break

    return data


def print_result_in_color(data):
    """Colorize specific keywords using pre-defined colors and print it."""
    print(colorize(data))


def printc(data, color=COLORS.BLUE):
    """Colorize data using a single color and print it."""
    print(f'{color}{data}{COLORS.RESET}')


LOGGER = file_logger(__name__)
LOGGER.printc = printc
LOGGER.print_result_in_color = print_result_in_color
