"""Restraint client protocol; wrapper around restraint client binary."""
import math
import os
import pathlib
import shlex
import time
import typing

from cki_lib import misc
from twisted.internet import defer
from twisted.internet import error
from twisted.internet import protocol
from twisted.internet import reactor
from twisted.internet import task as twisted_task

from upt import const
from upt.logger import COLORS
from upt.logger import LOGGER
from upt.logger import colorize
from upt.plumbing.interface import ProvisionerCore
from upt.restraint.file import RestraintJob
from upt.restraint.file import RestraintTask
from upt.restraint.wrapper import const as restraint_const
from upt.restraint.wrapper.actions import ActionOnResult
from upt.restraint.wrapper.host import RestraintHost
from upt.restraint.wrapper.misc import attempt_heartbeat_stop
from upt.restraint.wrapper.misc import attempt_reactor_stop
from upt.restraint.wrapper.misc import convert_path_to_link
from upt.restraint.wrapper.shell import ShellWrap
from upt.restraint.wrapper.task_result import TaskResult
from upt.restraint.wrapper.watcher import Watcher

from .dataclasses import ConsoleTask
from .dataclasses import TaskExecutionEnvironment


class RestraintClientProcessProtocol(protocol.ProcessProtocol):
    # pylint: disable=too-many-instance-attributes,disable=too-many-public-methods
    """Restraint binary processor."""

    def __init__(
        self,
        provisioner: ProvisionerCore,
        resource_group,
        add_protocol,
        on_task_result,
        **kwargs
    ):
        """Create the object."""
        self.restraint_binary = 'restraint'
        self.provisioner = provisioner
        self.resource_group = resource_group

        self.add_protocol = add_protocol

        self.kwargs = kwargs
        self.dump = kwargs['dump']
        # Global output directory.
        self.output = kwargs['output']

        # Optional method to call when a task has finished executing.
        self.on_task_result = on_task_result

        # Subdirectory created by restraint client, e.g './job.01'
        self.run_suffix = None

        self.cleanup_done = False

        # Generate shell commands and possibly ssh aliases for restraint
        # client. These are to be kept in a temporary directory, to allow
        # inspection and debugging.
        self.wrap = ShellWrap(resource_group, **self.kwargs)

        # Here are hosts that we're processing input from.
        # Load expected hosts to improve parsing of restraint output
        self.rst_hosts = RestraintHost.from_line(self.wrap.restraint_commands)

        # craft output file prefix
        self.xml_name = const.RSTRNT_JOB_XML.replace('.xml', '')

        # Check if this execution has multihost
        self.is_multihost = self._is_multihost()

        # How many times can we re-run a task
        self.reruns = self._get_reruns()

        # This watches subtask results.
        self.watcher = Watcher()

        self.recipe_ids_dead = set()

        self.result_actions = self._prepare_actions()

        # twisted restraint client process
        self.proc = None
        self.heartbeat_loop = None

        # The last task result that was processed, may belong to any host in resource group.
        self.last_task_result = None

        # Save yet unprocessed restraint output until we receive a full line
        self.unprocessed_out_data = ''

    def _is_multihost(self):
        """Is a multihost."""
        # Basically a multi-host recipeset contains more than one host.
        return len(self.rst_hosts) > 1

    def _get_reruns(self) -> int:
        """
        Avoid reruns globally.

        https://gitlab.com/cki-project/upt/-/issues/134
        """
        if self.kwargs['reruns'] > 0:
            LOGGER.printc('UPT temporarily does not have reruns !!!', color=COLORS.YELLOW)
        return 0

    def host_heartbeat(self):
        """Possibly stop processing on all hosts that failed heartbeat."""
        # Get a list of hostnames that provisioner thinks are "dead".
        LOGGER.debug("heartbeat!")
        self.provisioner.heartbeat(self.resource_group, self.recipe_ids_dead)

        for host in self.resource_group.recipeset.hosts:
            if host.recipe_id in self.recipe_ids_dead or host.panicked:
                LOGGER.info('* Marking %i as dead -> restraint client process will end',
                            host.recipe_id)
                # We only care about setting this to hosts that aren't finished
                # processing. If the host is done processing, no changes will
                # be made.
                self.evaluate_host_abort(host)

                self.handle_failure()
                attempt_heartbeat_stop(self.heartbeat_loop)

        return self.recipe_ids_dead

    def outReceived(self, data):
        """Process incoming stdout data from restraint client."""
        data = data.decode('utf-8')
        LOGGER.debug(data)

        if not self.heartbeat_loop:
            self.heartbeat_loop = twisted_task.LoopingCall(self.host_heartbeat)
            self.heartbeat_loop.start(60.0)

        # run heartbeat check on connectivity issues
        defer_heartbeat = defer.Deferred()
        defer_heartbeat.addErrback(self.handle_failure)
        defer_heartbeat.callback(data)

        self.unprocessed_out_data += data

        lines = []
        if self.unprocessed_out_data.endswith('\n'):
            # We have full line(s), nothing is left over
            lines = self.unprocessed_out_data.splitlines()
            self.unprocessed_out_data = ''
        else:
            # The last part is not a full line and we need to keep it around
            try:
                last_newline = self.unprocessed_out_data.rindex('\n')
            except ValueError:
                # Not a single finished line, nothing to do
                return

            lines = self.unprocessed_out_data[:last_newline].splitlines()
            self.unprocessed_out_data = self.unprocessed_out_data[last_newline + 1:]

        for line in lines:
            # Handle a case when a host becomes unresponsive
            match = RestraintHost.rgx_disconnect.match(line)

            # Last ssh connection retry implies issues => do heartbeat, if heartbeat fails,
            # evaluate last task as if it aborted using actions in _prepare_actions that codify
            # rules used to evaluate results.
            if match and match.group(1) == match.group(2):
                self.host_heartbeat()
            else:
                self.find_host_lines(line)

    def handle_failure(self, failure=None):
        """Log exception and terminate restraint client."""
        if failure:
            LOGGER.warning("Fatal exception caught %s", failure.getTraceback())
        else:
            LOGGER.warning('Host died, killing restraint process!')
        # Don't stop reactor, that would stop all the instances. Terminate the
        # restraint client process, if it isn't already.
        try:
            self.proc.signalProcess('TERM')
        except error.ProcessExitedAlready:
            pass

    def _check_stderr(self, data):
        """Check restraint client stderr messages for error indications."""
        result = restraint_const.RGX_LAB_WATCHDOG.findall(data)
        for hit_recipe_id in result:
            LOGGER.print_result_in_color(f'*** Recipe {hit_recipe_id} hit lab watchdog!')
            self.recipe_ids_dead.add(int(hit_recipe_id))
            # Find the host object that hit lab watchdog.
            host = self.provisioner.find_host_object([self.resource_group], int(hit_recipe_id))
            self.evaluate_host_abort(host)
            return

        for err in restraint_const.ERROR_INDICES:
            if err.findall(data):
                LOGGER.error("Exiting because: %s", data)

                attempt_reactor_stop(reactor)

    def errReceived(self, data):
        """Process incoming stderr data from restraint client."""
        data = data.decode('utf-8')
        if data.strip():
            # Log all stderr data
            LOGGER.debug(data.strip())

        defer_stderr_handling = defer.Deferred()
        defer_stderr_handling.addCallback(self._check_stderr)
        defer_stderr_handling.addErrback(self.handle_failure)
        # check stderr output for fatal errors
        defer_stderr_handling.callback(data)

    def identify_host(self, line):
        """Identify host output in restraint client."""
        match_result = RestraintHost.rgx_host.fullmatch(line)
        if match_result:
            self.rst_hosts.append(RestraintHost(*match_result.groups()))
            return True
        return False

    def filter_output(self, line):
        """Get an output line that belongs to a certain hostname."""
        # look for starting lines about hostname/recipe_ids
        if self.identify_host(line):
            # "Connecting to" line; nothing else do be done
            return None

        new_run = restraint_const.RGX_NEW_RUN.match(line)
        if new_run:
            self.run_suffix = new_run.group(1)

            path2watch = os.path.join(f'{os.path.abspath(self.kwargs["output"])}',
                                      os.path.relpath(self.run_suffix))
            # Start watcher with exact path
            reactor.callInThread(self.watcher.run, path2watch)
            # Waiting 0.3 for the watcher
            time.sleep(0.3)

        # try to figure out to which host the output line belongs
        match_result = RestraintHost.rgx_host_line.findall(line)
        if not match_result:
            # one more line to filter out: info about where output is saved
            if 'Disconnected..' in line:
                LOGGER.info(line.strip())
                return None
            if 'for job run' in line:
                # Debug-logged by outReceived.
                return None

            # unknown output
            LOGGER.info('!!! Received unknown output "%s"', line)
            return None

        return match_result

    def split_hostline_match(self, host: str, hostline: str) \
            -> typing.Optional[typing.Tuple[RestraintHost, ConsoleTask]]:
        """Parse a line from a hostname."""
        idx = RestraintHost.in_list(host, self.rst_hosts)
        if idx is not None:
            hostobj = self.rst_hosts[idx]  # type: RestraintHost
            hostobj.lines.append(hostline)
            try:
                regex2use = RestraintHost.rgx_state_change
                task_id, testname, status, result = regex2use.findall(
                    hostline)[0]
                # Sanitize output !
                task_id = int(task_id)
            except (AttributeError, TypeError, IndexError):
                # This could be score line details like:
                # 81955317 [sanity                          ] PASS Score: 0
                if 'Listening' in hostline:
                    print(f'{hostobj.hostname:<40} {hostline}')
            else:
                return (hostobj,
                        ConsoleTask(task_id=task_id, task_name=testname.strip(),
                                    status=status, result=result)
                        )
        else:
            LOGGER.info('!!! Received line from unknown host %s', host)
            LOGGER.info('* line: "%s"', hostline)

        return None

    def find_host_lines(self, line: str) -> None:
        """Process restraint output."""
        match_result = self.filter_output(line)
        if not match_result:
            return

        for hostobj, hostline in match_result:
            tup = self.split_hostline_match(hostobj, hostline)
            if not tup:
                continue
            hostobj, console_task = tup
            # Ignore Abort that could cause a re-run.
            if console_task.status != 'Aborted' or hostobj.recipe_id not in self.host_heartbeat():
                LOGGER.debug('*** New restraint client output ***')
                host = self.provisioner.find_host_object([self.resource_group], hostobj.recipe_id)
                # Pass params to ActionOnResult objects, so we keep evaluating
                # result and do further actions, such as re-running some tasks.
                self.process_task(host, console_task)

    @classmethod
    def get_dumpfile_name(cls, task_result, attempts_made, resource_id, prefix=''):
        """Get kcidb dumpfile name given task_result."""
        # This is not to be used for anything else; it is not .path attribute.
        test_ident = (task_result.universal_id if task_result.universal_id else
                      f'redhat_{task_result.testname.lstrip("/")}').replace('/', '.')

        original_taskid = task_result.task_id if attempts_made == 0 else \
            task_result.host.rerun_recipe_tasks_from_index + task_result.task_id - 1

        return f'{prefix}{resource_id}_{attempts_made}_R_{task_result.recipe_id}_T_' \
               f'{original_taskid}_test-{test_ident}'

    def get_logs_results_dst_dir(self, task_result, dirname):
        """Return pathlib path for a task results/logs."""
        fname = self.get_dumpfile_name(task_result,
                                       self.resource_group.recipeset.attempts_made,
                                       self.resource_group.resource_id)

        dst_dir = pathlib.Path(self.output, task_result.host.counter.path,
                               f'{dirname.upper()}_{fname}')
        return dst_dir

    def cleanup_handler(self):
        # pylint: disable=bare-except
        """Ensure related threads stop."""
        if self.cleanup_done:
            return

        # NOTE: stop watcher first, any issue in handler above could leave the process stuck!
        self.watcher.observer.stop()

        # Client process ended, nothing to do. Reruns will have to be done by
        # another restraint protocol object.
        LOGGER.debug('restraint protocol cleanup runs (rid: %s)...',
                     self.resource_group.resource_id)

        recipeset = self.resource_group.recipeset
        recipeset.tests_finished = True
        do_rerun = False

        for host in recipeset.hosts:
            if not host.done_processing:
                self.evaluate_host_abort(host)

        if recipeset.waiting_for_rerun:
            # Increase the counter of attempts. Entire recipeSet has to be re-run.
            recipeset.attempts_made += 1
            do_rerun = recipeset.attempts_made <= self.reruns
            # Careful! Clear the flag AFTER evaluating re-run condition.
            recipeset.waiting_for_rerun = False

        if do_rerun:
            # Add a new protocol
            proto = self.add_protocol(self.provisioner, self.resource_group,
                                      self.on_task_result, **self.kwargs.copy())
            proto.start_all()
        else:
            # No rerun.
            # If we're not rerunning anything, then recipeset is finished.
            for host in recipeset.hosts:
                host.done_processing = True

                # Dump the remaining tasks w/ MISS kcidb_status after processing a host.
                if self.dump:
                    self.dump_remaining_tasks_with_miss_status(host)

            LOGGER.info('Releasing resources')
            self.provisioner.release_resources()

        attempt_heartbeat_stop(self.heartbeat_loop)

        self.cleanup_done = True

    def processEnded(self, reason):
        """Cleanup on process exit, handle retcode if it was caused by user."""
        # Log that restraint client process ended
        result = [f'R:{r.recipe_id}' for r in self.provisioner.find_objects(
            [self.resource_group], lambda obj: obj if hasattr(obj, 'recipe_id') else None)]

        was_sigpiped = 'ended by signal 13' in str(reason.value)
        LOGGER.info('* restraint protocol (%s) %s retcode: %s reason: %s',
                    result,
                    'was SIGPIPEd!' if was_sigpiped else 'ended.',
                    reason.value.exitCode,
                    reason.value)
        if was_sigpiped:
            lasttr = self.last_task_result
            msg = (f'{COLORS.RED}restraint protocol was killed by SIGPIPE, '
                   f'the test may be misbehaving{COLORS.RESET}')
            rich_msg = ActionOnResult.format_msg(
                lasttr.recipe_id, lasttr.task_id, msg, '*', lasttr.testname) if lasttr else msg
            print(rich_msg)

        self.cleanup_handler()

    def rerun_from_task(self, **kwargs):
        """Ensure task(s) are re-run for a host."""
        host = kwargs['host']
        recipeset = self.resource_group.recipeset

        if any(rs_host.done_processing for rs_host in recipeset.hosts):
            # Multihost testing cannot be rerun if any of the hosts is dead!
            LOGGER.info(
                'At least one of the hosts of the recipeset is dead, no reruns can be done!'
            )

            # Mark all hosts as done since we can't reliably run anything now.
            for rs_host in recipeset.hosts:
                rs_host.done_processing = True

            # Explicitly mark the recipeset as not waiting for reruns in case
            # the value was set previously.
            recipeset.waiting_for_rerun = False

            return

        # Set waiting_for_rerun and let tests finish, if they can.
        if not recipeset.waiting_for_rerun:
            recipeset.waiting_for_rerun = True
            # Restraint numbers task_id from 1.
            host.rerun_recipe_tasks_from_index = kwargs['task_id'] \
                if host.rerun_recipe_tasks_from_index is None \
                else host.rerun_recipe_tasks_from_index + kwargs['task_id'] - 1

            task_result = kwargs['task_result']

            statusresult = f'Making a re-run attempt ' \
                           f'{recipeset.attempts_made + 1}/{self.kwargs["reruns"]} for the' \
                           f' entire recipeSet'
            msg = ActionOnResult.format_msg(task_result.recipe_id, task_result.task_id,
                                            statusresult, '*', task_result.testname)
            LOGGER.info(msg)

    def add_task_result(self, host: RestraintHost,
                        restraint_task: RestraintTask,
                        console_task: ConsoleTask) \
            -> TaskResult:
        """Save task results to a RestraintHost object."""
        ewd_hit = self.is_ewd_hit(host.recipe_id, console_task.status)
        environment = TaskExecutionEnvironment(is_ewd_hit=ewd_hit,
                                               is_multihost=self.is_multihost,
                                               restraint_output_path=pathlib.Path(
                                                   self.output, self.run_suffix
                                               ))
        task_result = TaskResult(host, restraint_task, console_task, environment)

        host.task_results.append(task_result)

        return task_result

    @classmethod
    def cond_print_notification(cls, task_result):
        """When a test didn't pass, print carefully colorized info."""
        # "ERROR", "FAIL", "PASS", "DONE", "SKIP", "MISS"
        if task_result.kcidb_status in ("ERROR", "FAIL"):
            color = COLORS.RED if task_result.kcidb_status == "FAIL" and not task_result.waived \
                else COLORS.YELLOW

            maintainers = [f'{entry["name"]} <{entry["email"]}>' for entry in
                           task_result.test_maintainers]
            gitlab_nicks = [f'{entry["gitlab"]}' for entry in task_result.test_maintainers if
                            entry["gitlab"]]
            gitlab_nicks = " ".join(gitlab_nicks) if gitlab_nicks else "<NOT SET!>"
            maintainers = " ".join(maintainers) if task_result.test_maintainers else \
                "<NOT SET!>"
            waive_info = 'waived' if task_result.waived else 'NOT WAIVED'
            LOGGER.printc(f'Test "{task_result.testname}" {task_result.kcidb_status}ed'
                          f' (result={task_result.result}, status={task_result.status})'
                          f' and it is {waive_info}. Please check with following test maintainers'
                          f' if you have trouble figuring out the failure: {maintainers}.'
                          f' gitlab.com usernames of the maintainers are {gitlab_nicks}',
                          color=color)

            if task_result.output_path:
                dir_link = convert_path_to_link(task_result.output_path, False)
                LOGGER.printc(f'Test logs will be in: {dir_link}', color=COLORS.BLUE)
            else:
                LOGGER.printc('Unable to find test logs directory', color=COLORS.BLUE)

    def mark_recipe_done(self, **kwargs):
        """Find host by recipe_id and mark it as done processing.

        Whenever this method is called, it will mark host as 'done_processing' and no more
        tests will be run on the host.
        """
        task_result = kwargs['task_result']
        recipe_id = task_result.recipe_id

        if task_result.recipe_id != kwargs['recipe_id']:
            raise RuntimeError('broken code: invalid data provided')

        host = self.provisioner.find_host_object([self.resource_group], recipe_id)
        if not host.done_processing:
            host.done_processing = True

            msg = ActionOnResult.format_msg(host.recipe_id, task_result.task_id,
                                            'recipe is done processing [mark_recipe_done]', '*',
                                            task_result.testname)
            LOGGER.debug(msg)

        # Ensure no reruns are attempted if this function is called as a result
        # of aborted or panicked hosts. Those are broken and the reruns will
        # immediately fail. If this function is called as part of a finished
        # run cleanup, then the following assignment is a noop.
        self.resource_group.recipeset.waiting_for_rerun = False

    @classmethod
    def add_panic_subtest(cls, **kwargs):
        """Add a panic subtest."""
        task_result = kwargs['task_result']
        task_result.add_panic_subtest()

    def _prepare_actions(self):
        # Determines what actions to make on a specific result combination.
        # See example use comment in the object itself.
        return [
            ActionOnResult('FAIL', [self.mark_recipe_done],
                           '(EWD hit during kernel boot means kernel boot failure.'
                           ' Further testing would be invalid -> recipe processing finished.)',
                           ewd_hit={True}, testname={'Boot test'}),

            # Panic detection comes from Beaker and NOT from restraint, thus it uses Beaker's
            # result format with only the first letter capitalized!
            ActionOnResult('FAIL', [self.add_panic_subtest, self.mark_recipe_done],
                           '(Panic -> recipe processing finished.)',
                           result={'Panic'}, status={'Aborted'}),

            ActionOnResult('ERROR', [self.mark_recipe_done],
                           '(LWD hit during kernel boot means kernel not installed correctly.'
                           ' Further testing would be invalid -> recipe processing finished.)',
                           lwd_hit={True}, testname={'Boot test'}),

            ActionOnResult('FAIL', [],
                           '(EWD was hit, but the test reported failure. No reruns will be done.)',
                           ewd_hit={True}, status={'Aborted'}, result={'FAIL'}),

            ActionOnResult('ERROR', [self.mark_recipe_done],
                           '(EWD hit, but no Panic is an infrastructure issue.'
                           ' No reruns will be done. Infrastructure issue -> recipe processing'
                           ' finished.)',
                           ewd_hit={True}, status={'Aborted'}),

            ActionOnResult('ERROR', [],
                           '(LWD hit. This type of issue is ignored completely.)',
                           lwd_hit={True}),

            ActionOnResult('ERROR', [self.rerun_from_task],
                           '(A task aborted. Re-running task to confirm infrastructure issue.)',
                           status={'Aborted'}, out_of_reruns={False},
                           waiting_for_rerun={False}),

            # There is an issue requesting restraint to support this
            # https://github.com/restraint-harness/restraint/issues/288
            ActionOnResult('ERROR', [self.mark_recipe_done],
                           'Boot test did not finish.'
                           ' Further testing would be invalid -> recipe processing finished.)',
                           status={'Aborted'}, testname={'Boot test'}),

            ActionOnResult('ERROR', [],
                           '(A task aborted but a rerun was already scheduled. Infrastructure'
                           ' issue.)',
                           result={'WARN'}, status={'Aborted'}),

            # https://gitlab.com/cki-project/upt/-/issues/131
            ActionOnResult('ERROR', [],
                           '(A task aborted with result "PASS", marking it as "ERROR")',
                           result={'PASS'}, status={'Aborted'}),

            ActionOnResult('ERROR', [],
                           '(A task has warnings, marking it as "ERROR")',
                           result={'WARN'}),

            ActionOnResult('FAIL', [],
                           '(A task failed. Kernel testing failed.)',
                           result={'FAIL'}),

            ActionOnResult('PASS', [],
                           '(A task passed.)',
                           result={'PASS'}),

            ActionOnResult('SKIP', [],
                           '(A task was skipped.)',
                           result={'SKIP'}),

            ActionOnResult('MISS', [],
                           '(A task aborted without running, likely the whole run was aborted.)',
                           status={'Aborted'}),
            ActionOnResult(None, [],
                           '',  # No explanation to log, format_msg is enough
                           status={'Running'})
        ]

    def is_ewd_hit(self, recipe_id, status):
        """Check if a given recipe_id had an EWD hit."""
        LOGGER.debug('is_ewd_hit: %i %s %s', recipe_id, status, self.recipe_ids_dead)
        return recipe_id in self.recipe_ids_dead

    def is_last_host_task(self, recipe_id, task_id):
        """Return True if this is the last task that the host should run.

        Arguments:
            recipe_id - int, the id that uniquely identifies the host
        """
        restraint_job = RestraintJob.create_from_string(
            self.resource_group.recipeset.restraint_xml
        )
        restraint_recipe = restraint_job.get_recipe_by_id(recipe_id)

        assert restraint_recipe is not None, \
            "Invalid data or broken code. Cannot find recipe_id specified."

        return len(restraint_recipe.tasks) == int(task_id)

    def dump_remaining_tasks_with_miss_status(self, host):
        """Dump remaining tasks with MISS status."""
        attempts_made = self.resource_group.recipeset.attempts_made
        resource_id = self.resource_group.resource_id
        for task in host.planned_tests:
            task_result = next((result for result in host.task_results
                                if result.cki_name == task.cki_name), None)
            if task_result:
                # This task already ran and was dumped
                continue

            task.status = 'Cancelled'
            task.kcidb_status = 'MISS'
            fname = self.get_dumpfile_name(task,
                                           attempts_made,
                                           resource_id,
                                           prefix='kcidb_') + '.json'
            LOGGER.debug('R: %s T: %s name: %s missed',
                         task.host.recipe_id,
                         task.task_id,
                         task.testname)
            self.on_task_result(self.resource_group, task, fname)

    def use_action_rules(self, task_result, host, ac_kwargs):
        """Determine what should happen based on the results and data about hosts we have."""
        # Go through predefined actions based on results
        for action in self.result_actions:
            # Check if conditions are fulfilled.
            if action.eval(**ac_kwargs):
                # Debug: store data about what the conditions were
                LOGGER.debug(str(action))

                # Set sanitized kcidb test status/result!
                task_result.kcidb_status = action.kcidb_status
                # Format and print result.
                msg = action.format_msg(host.recipe_id, task_result.task_id,
                                        colorize(f'{task_result.statusresult:<20}'),
                                        '*',
                                        task_result.testname, base=action.msg_string)
                print(msg)

                # If so, run all methods that must run in this case.
                for func in action.lst_actions:
                    func(**ac_kwargs)

                if task_result.kcidb_status:  # Do we have a finished task?

                    # On error: do notification with highlighting and maintainer info print.
                    self.cond_print_notification(task_result)

                # DO NOT PROCESS OTHER ACTIONS IN THE LIST
                break

        if task_result.kcidb_status:
            # Dump json file(s) anytime there's new task done!
            if self.dump:
                fname = self.get_dumpfile_name(
                    task_result,
                    self.resource_group.recipeset.attempts_made,
                    self.resource_group.resource_id, prefix='kcidb_'
                ) + '.json'
                self.on_task_result(self.resource_group, task_result, fname)

    def evaluate_task_result(self, task_result, host):
        """Evaluate the result of task_result in relation to a host."""
        ac_kwargs = {'recipe_id': task_result.recipe_id,
                     'task_id': task_result.task_id,
                     'testname': task_result.testname,
                     'task': task_result.restraint_task,
                     'status': task_result.status,
                     'result': task_result.result,
                     'waived': task_result.waived,
                     'lwd_hit': task_result.lwd_hit,
                     'ewd_hit': task_result.ewd_hit,
                     'host': host,
                     'task_result': task_result,
                     'out_of_reruns': (self.resource_group.recipeset.attempts_made >= self.reruns),
                     'is_last_host_task': self.is_last_host_task(task_result.recipe_id,
                                                                 task_result.task_id),
                     'waiting_for_rerun': self.resource_group.recipeset.waiting_for_rerun}

        # Save what ran last.
        self.last_task_result = task_result

        if self.is_last_host_task(task_result.recipe_id, task_result.task_id) and \
                task_result.status and task_result.result and \
                not self.resource_group.recipeset.waiting_for_rerun:
            task_result.host.done_processing = True
            self.mark_recipe_done(**ac_kwargs)

        if not task_result.is_cki_test:
            statusresult = task_result.status if not task_result.result else\
                f'{task_result.status}: {task_result.result}'
            status_result_in_color = colorize(f'{statusresult:<20}')
            print(ActionOnResult.format_msg(host.recipe_id, task_result.task_id,
                                            status_result_in_color, '*', task_result.testname))

            print(f'{task_result.testname} is a setup task - not evaluating as test.')
            return

        # Go through a list rules and determine what should happen based on the results and data
        # about hosts we have.
        self.use_action_rules(task_result, host, ac_kwargs)

    def get_restraint_xml_task(self, recipe_id, task_id):
        """
        Get task of a recipe by recipe_id and task_id.

        We can get the source from two different origins:
          * Running restraint file
          * Original restraint file

        Try to get first from the running file, and we can't get
        the running file, try to get from the original file.

        This method should be updated to use CKI_ID when it's ready.
        """
        original_recipe = self.wrap.run_restraint_job.get_recipe_by_id(recipe_id)

        if (running_restraint_job := self.watcher.get_job()):
            running_recipe = running_restraint_job.get_recipe_by_id(recipe_id)
            try:
                return running_recipe.tasks[task_id - 1]
            except (IndexError, AttributeError):
                LOGGER.warning('task T:%i not found in the recipe %i in the running restraint',
                               task_id, recipe_id)
        LOGGER.info('Unable to get the task status from running restraint,'
                    ' trying to get the task from the original file,'
                    ' it should happens only with the first task')
        try:
            return original_recipe.tasks[task_id - 1]
        except (IndexError, AttributeError):
            LOGGER.warning('task T:%i not found in the recipe %i from original restraint!',
                           task_id, recipe_id)
        LOGGER.error('task T:%i not found in the recipe %i!', task_id, recipe_id)
        return None

    def evaluate_host_abort(self, host):
        """Evaluate the result of the last task again, but override its status to 'Aborted'.

        This is done because restraint gives us output on task state transition
        only and we need to re-evaluate conditions, when the host suddenly hits
        EWD. This method is to be called in such cases.
        """
        forced_status = 'Aborted'
        forced_result = 'Panic' if host.panicked else None
        if not host.task_results:
            # If nothing was run, get a task that was supposed to be run. Here
            # we have some chance that the distro failed to install.
            restraint_task = self.get_restraint_xml_task(host.recipe_id, 1)
            if not restraint_task:
                LOGGER.error('💀 Unhandled case: cannot find task_id 1!')
                return

            # NOTE-hack: we quietly assume that the task Aborted along with the
            # system, but restraint wasn't able to provide this info.
            console_task = ConsoleTask(task_id=1, task_name=restraint_task.name,
                                       result=forced_result, status=forced_status)
            task_result = self.add_task_result(host, restraint_task, console_task)
        else:
            # Get last task that was run and its task_result. If the task
            # started running at all and there was an output from restraint,
            # then the result should be available.
            task_result = host.task_results[-1]
            task_result.status = forced_status
            task_result.result = forced_result or task_result.result
            task_result.statusresult = forced_status if not task_result.result else \
                f'{forced_status}: {task_result.result}'
            restraint_task = task_result.restraint_task
            task_result.ewd_hit = self.is_ewd_hit(task_result.recipe_id, task_result.status)

        # Run method to evaluate conditions and run appropriate action.
        self.evaluate_task_result(task_result, host)

    def process_task(self, host: RestraintHost, console_task: ConsoleTask) \
            -> typing.Optional[RestraintTask]:
        """Evaluate the result of the task and how it affects how we go on.

        This method is to be called immediately after we've received restraint client output.
        """
        restraint_task = self.consolidate_restraint_file_and_console(host.recipe_id, console_task)
        if not restraint_task:
            LOGGER.error('R:%i  T:%i to rerun was not found!', host.recipe_id,
                         console_task.task_id)
            return None

        # Save task result in natural order per recipe_id
        task_result = self.add_task_result(host, restraint_task, console_task)

        return self.evaluate_task_result(task_result, host)

    def consolidate_restraint_file_and_console(self, recipe_id: int, console_task: ConsoleTask,
                                               timeout: int = 2) \
            -> typing.Optional[RestraintTask]:
        """Wait until the status is consolidated between restraint file and console.

        The consolidation between the console (TTY) and the restraint file have some delays,
        we only care about when the console status is Completed or Aborted.
        """
        # Getting the task
        restraint_task = self.get_restraint_xml_task(recipe_id, console_task.task_id)

        # If it can not get the task, there is a problem, before it's unable to get the original
        # restraint task
        if not restraint_task:
            LOGGER.error('Unable to get the restraint task!')
            return None

        wait_interval = 0.1
        # If we receive an invalid timeout, setting to the default
        if timeout <= 0:
            LOGGER.error('The timeout value (%i) is invalid, setting to 2 seconds', timeout)
            timeout = 2
        attempts = math.floor(timeout / wait_interval)

        if console_task.status in ('Completed', 'Aborted'):
            for attempt in range(1, attempts+1):
                LOGGER.debug('Attempt %i of %i', attempt, attempts)
                if str(restraint_task.status.upper()) == console_task.status.upper():
                    LOGGER.debug(
                        'The task status is synced between console and restraint file'
                    )
                    break
                LOGGER.debug(
                    'Task status out of sync between console and restraint file (%s != %s)',
                    console_task.status, restraint_task.status
                )
                time.sleep(wait_interval)
                restraint_task = self.get_restraint_xml_task(recipe_id, console_task.task_id)

        return restraint_task

    def start_all(self):
        """Create & start process, register cleanup handlers."""
        cmds = self.wrap.restraint_commands
        print(f'* Running "{cmds}"...')

        # change directory, return on context end
        with misc.enter_dir(self.output):
            self.proc = reactor.spawnProcess(
                self, self.restraint_binary, shlex.split(cmds), env=os.environ)

            reactor.addSystemEventTrigger('before', 'shutdown', self.cleanup_handler)
