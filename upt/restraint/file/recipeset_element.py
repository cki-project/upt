"""Restraint RecipeSet Element."""

from __future__ import annotations

import copy
import typing
import xml.etree.ElementTree as ET

from .common_element import CommonXMLElement
from .recipe_element import RestraintRecipe
from .restraint_dataclasses import RestraintRecipeSetDiff


class RestraintRecipeSet(CommonXMLElement):
    """RecipeSet class.

    This class contains a list of recipes
    You can see an example in examples.py file (recipeset_example).
    """

    _attributes = ['id']
    _name = 'recipeSet'
    _delegated_items = ['recipe']

    def __init__(self, element: ET.Element):
        """Initialize."""
        super().__init__(element)
        self.recipes = self._process_recipes()
        self._clear()

    def _process_recipes(self) -> typing.List[RestraintRecipe]:
        """Process recipes."""
        return [RestraintRecipe(recipe) for recipe in self.element.findall('.//recipe')]

    def generate_xml_object(self) -> ET.Element:
        """Generate the object."""
        # clean the element
        self._clear()
        # Generate and add recipes
        for recipe in self.recipes:
            self.element.append(recipe.generate_xml_object())
        return copy.copy(self.element)

    def get_recipe_by_id(self, recipe_id: int) -> typing.Optional[RestraintRecipe]:
        """Get RestraintRecipe by id.

        Args:
            recipe_id - int, the id that uniquely identifies the RestraintRecipe.
        """
        return next((recipe for recipe in self.recipes
                     if int(recipe.id) == recipe_id), None)  # type: ignore

    def diff(self, other: RestraintRecipeSet) -> typing.Optional[RestraintRecipeSetDiff]:
        """Generate a diff between two recipesets, including recipe diff.

        A RestraintRecipeSet never should change the number of recipes,
        that's why we don't compare the number of recipes and its id,
        in the same RestraintRecipeSet.

        RestraintRecipeSet only has id attribute, so we can't compare its attribute, we only
        can compare recipes (delegated item).

        RestraintRecipeSet usually does not have id in job.xml files, so we don't check
        the id field.
        """
        if not isinstance(other, RestraintRecipeSet):
            raise TypeError('Both objects must be RestraintRecipeSet')

        recipes_changes = [
            recipe_changes
            for recipe, other_recipe in zip(self.recipes, other.recipes)
            if (recipe_changes := recipe.diff(other_recipe))
        ]

        if recipes_changes:
            return RestraintRecipeSetDiff(self.id, [], recipes_changes)

        return None
