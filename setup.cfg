[metadata]
name = upt
description = Unified Provisioning Tool
long_description = file: README.md
version = 0.0.1
author = CKI Team
author_email = cki-project@redhat.com
license = GPLv3

[options]
# Automatically find all packages
packages = find:
# Parse the MANIFEST.in file and include those files, too.
include_package_data = True
python_requires = >=3.10
install_requires =
    boto3
    cryptography
    jinja2
    python-gitlab
    PyYAML
    ruamel.yaml  # Needed until pyyaml can handle yaml 1.2 or we move to a different format
    sentry-sdk
    twisted
    watchdog
    cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git@production
    tf-requests @ git+https://gitlab.com/redhat/edge/ci-cd/pipe-x/tf-requests@1.0.2

[options.extras_require]
dev =
    cki-lib[lint] @ git+https://gitlab.com/cki-project/cki-lib.git@production
    freezegun
    mypy
    responses

[options.packages.find]
# Don't include the /tests directory when we search for python files.
exclude =
    tests*

[options.entry_points]
# Set up an executable 'upt' and 'rcclient' that call the main() function in
# __main__.py of their respective packages.
console_scripts =
    upt = upt.__main__:main
    rcclient = upt.restraint.wrapper.__main__:main

[tox:tox]

[pylint.typecheck]
ignored-modules=twisted*,

[pylint.messages_control]
disable = duplicate-code
# Good variable names which should always be accepted, separated by a comma.
good-names=i,
           j,
           id

[flake8]
per-file-ignores =
    upt/restraint/file/__init__.py:F401
    upt/restraint/file/examples.py:E501

[testenv]
extras =
    dev
setenv =
    AWS_DEFAULT_REGION = us-east-1
commands =
    cki_lint.sh upt
    cki_test.sh upt

[testenv:lint]
commands = cki_lint.sh upt

[testenv:test]
commands = cki_test.sh upt

[coverage:run]
omit =
    upt/restraint/file/examples.py

[mypy]
files = upt/restraint/file/*.py,
        upt/restraint/wrapper/dataclasses.py,
        upt/restraint/wrapper/task_result.py,
        upt/smoke_test/*.py
exclude = upt/restraint/file/examples.py

[mypy-cki_lib.*]
# missing type hints/library stub
ignore_missing_imports = True


# UPT code without typing modification that causing errors
# when checking upt code with typing enabled.
[mypy-upt.logger]
ignore_errors = True

[mypy-upt.misc]
ignore_errors = True

[mypy-upt.plumbing.*]
ignore_errors = True

[mypy-upt.restraint.wrapper.misc]
ignore_errors = True
